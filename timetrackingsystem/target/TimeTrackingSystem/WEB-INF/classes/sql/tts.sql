-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tts
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tts
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tts` DEFAULT CHARACTER SET utf8 ;
USE `tts` ;

-- -----------------------------------------------------
-- Table `tts`.`project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tts`.`project` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(225) NOT NULL,
  `responsible` VARCHAR(225) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tts`.`task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tts`.`task` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `projectid` INT NOT NULL,
  `name` VARCHAR(225) NOT NULL,
  `responsible` VARCHAR(225) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_task_project_idx` (`projectid` ASC),
  CONSTRAINT `fk_task_project`
    FOREIGN KEY (`projectid`)
    REFERENCES `tts`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tts`.`timerecord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tts`.`timerecord` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `taskid` INT NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `date` DATE NOT NULL,
  `hours` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_timerecord_task1_idx` (`taskid` ASC),
  CONSTRAINT `fk_timerecord_task1`
    FOREIGN KEY (`taskid`)
    REFERENCES `tts`.`task` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
