<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB_INF/tld/taglib.tld" prefix="tg"%>

<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locale/locale" scope="application" />

<html lang="language">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><fmt:message key="jsp.records.page.title" /></title>
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/bootstrap/css/sticky-footer-navbar.css"
	rel="stylesheet">

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>

	<jsp:include page="base/header.jsp" />

	<div class="container">
		<div class="page-header">
			<h1>
				<fmt:message key="jsp.records.repord.greeting" />
				<c:out value="${startdate}" />
				<fmt:message key="jsp.records.repord.greeting.end" />
				<c:out value="${enddate}" />
			</h1>
		</div>

		<!-- вывод сообщения об ошибке -->
		<!-- ПОЛЬЗОВАТЕЛЬСКИЙ ТЭГ -->
		<tg:jspmsg message="${reportMsg}" />

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><fmt:message key="jsp.record.date" /></th>
					<th><fmt:message key="jsp.user.firstname" /></th>
					<th><fmt:message key="jsp.user.lastname" /></th>
					<th><fmt:message key="jsp.record.hours" /></th>
					<th><fmt:message key="jsp.record.task" /></th>
					<th><fmt:message key="jsp.record.project" /></th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${recordsList.size()>0}">
					<c:forEach var="i" begin="0" end="${recordsList.size()-1}">
						<tr>
							<td><c:out value="${recordsList.get(i).date}" /></td>
							<td><c:out value="${recordsList.get(i).firstName}" /></td>
							<td><c:out value="${recordsList.get(i).lastName}" /></td>
							<td><c:out value="${recordsList.get(i).hours}" /></td>
							<td><c:out value="${tasksList.get(i).name}" /></td>
							<td><c:out value="${projectsList.get(i).name}" /></td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>

		<form name="createRecordReportForm" method="POST" action="controller">
			<div class="col-xs-4">
				<label for="startdate" class="control-label col-sm-3"> <fmt:message
						key="jsp.record.startdate" />
				</label>
				<div class="col-sm-6">
					<input type="text" class="form-control input-lg" id="startdate"
						name="startdate" value="" autofocus="autofocus" />
				</div>
			</div>
			<div class="col-xs-4">
				<label for="enddate" class="control-label col-sm-3"> <fmt:message
						key="jsp.record.enddate" />
				</label>
				<div class="col-sm-6">
					<input type="text" class="form-control input-lg" id="enddate"
						name="enddate" value="" autofocus="autofocus" />
				</div>
			</div>
			<input type="hidden" name="command" value="create_record_report" />
			<input class="btn-lg" type="submit"
				value="<fmt:message key="jsp.record.create" />" />
		</form>

		<c:if test="${recordsList.size()>0}">
			<form name="downloadRecordReportForm" method="POST"
				action="controller">
				<input type="hidden" name="command" value="download_record_report" />
				<input class="btn-lg" type="submit"
					value="<fmt:message key="jsp.record.download" />" />
			</form>
		</c:if>



	</div>

	<jsp:include page="/jsp/base/footer.jsp" />
</body>