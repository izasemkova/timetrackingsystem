<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB_INF/tld/taglib.tld" prefix="tg"%>

<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locale/locale" scope="application" />

<html lang="language">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><fmt:message key="jsp.manager.page.title" /></title>
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/bootstrap/css/sticky-footer-navbar.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>

	<script
		src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
	<script
		src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

	<jsp:include page="base/header.jsp" />

	<div class="container">
		<div class="page-header">
			<h1>
				<fmt:message key="jsp.manager.page.greeting" />
				<c:out value="${user.firstName}" />
			</h1>
		</div>
		<p class="lead">
			<fmt:message key="jsp.manager.page.text" />
		</p>

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><fmt:message key="jsp.project.name" /></th>
					<th><fmt:message key="jsp.project.responsible" /></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${projectsList}" var="project">
					<tr>
						<td><c:out value="${project.name}" /></td>
						<td><c:out value="${project.responsible}" /></td>
						<td><c:if test="${not empty project.tasksList}">
								<form name="getTasksForm" method="POST" action="controller">
									<input type="hidden" name="command" value="manager_tasks_page" />
									<input type="hidden" name="projectId" value="${project.id}" />
									<input class="btn btn-link" type="submit"
										value="<fmt:message key="jsp.project.get.tasks" />" />
								</form>
							</c:if></td>
						<td>
							<form name="setResponsibleForm" method="POST" action="controller">
								<input type="hidden" name="command" value="update_project" /> <input
									type="hidden" name="projectId" value="${project.id}" /> <input
									class="btn btn-link" type="submit"
									value="<fmt:message key="jsp.manager.page.set.responsible" />" />
							</form>
						</td>
						<td>
							<form name="addTaskForm" method="POST" action="controller">
								<input type="hidden" name="command" value="create_task" /> <input
									type="hidden" name="projectId" value="${project.id}" /> <input
									class="btn btn-link" type="submit"
									value="<fmt:message key="jsp.manager.page.add.task" />" />
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>

		</table>

		<form name="addProjectForm" method="POST" action="controller">
			<input type="hidden" name="command" value="create_project" /> <input
				class="btn-lg" type="submit"
				value="<fmt:message key="jsp.manager.page.add.project" />" />
		</form>

		<form name="createTimeReportForm" method="POST" action="controller">
			<input type="hidden" name="command" value="time_report_page" /> <input
				class="btn-lg" type="submit"
				value="<fmt:message key="jsp.manager.page.add.time.report" />" />
		</form>
		<form name="createRecordReportForm" method="POST" action="controller">
			<input type="hidden" name="command" value="record_report_page" /> <input
				class="btn-lg" type="submit"
				value="<fmt:message key="jsp.manager.page.add.record.report" />" />
		</form>
	</div>

	<jsp:include page="/jsp/base/footer.jsp" />
</body>