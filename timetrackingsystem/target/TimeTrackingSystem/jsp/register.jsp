<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB_INF/tld/taglib.tld" prefix="tg"%>

<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locale/locale" scope="application" />

<html lang="language">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><fmt:message key="jsp.register.user.title" /></title>
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/bootstrap/css/sticky-footer-navbar.css"
	rel="stylesheet">

<SCRIPT type="text/javascript">
<!--
	function validateForm() {
		valid = true;
		if (document.newUserForm.login.value == "") {
			alert("Пожалуйста заполните поле 'Login'.");
			valid = false;
		}
		if (document.newUserForm.password.value == "") {
			alert("Пожалуйста заполните поле 'Password'.");
			valid = false;
		}
		if (document.newUserForm.firstName.value == "") {
			alert("Пожалуйста заполните поле 'First name'.");
			valid = false;
		}
		if (document.newUserForm.lastName.value == "") {
			alert("Пожалуйста заполните поле 'Last name'.");
			valid = false;
		}
		return valid;
	}
//-->
</SCRIPT>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>

	<jsp:include page="base/header.jsp" />

	<div class="container">
		<div class="page-header">
			<h1>
				<fmt:message key="jsp.register.user.greeting" />
			</h1>
		</div>

		<!-- вывод сообщения об ошибке -->
		<!-- ПОЛЬЗОВАТЕЛЬСКИЙ ТЭГ -->
		<tg:jspmsg message="${newUserMsg}" />


		<form name="newUserForm" method="POST" action="controller"
			class="form-horizontal" role="form" onsubmit="return validateForm();">
			<input type="hidden" name="command" value="save_user" />
			<div class="form-group form-group-lg ">
				<label for="login" class="control-label col-sm-2"> <fmt:message
						key="jsp.user.login" />
				</label>
				<div class="col-sm-3">
					<input type="text" class="form-control input-lg" id="login"
						name="login" value="${newUser.login}">
				</div>
			</div>
			<div class="form-group form-group-lg ">
				<label for="passsword" class="control-label col-sm-2"> <fmt:message
						key="jsp.user.password" />
				</label>
				<div class="col-sm-3">
					<input type="password" class="form-control input-lg" id="password"
						name="password" value="${newUser.password}">
				</div>
			</div>
			<div class="form-group form-group-lg ">
				<label for="firstName" class="control-label col-sm-2"> <fmt:message
						key="jsp.user.firstname" />
				</label>
				<div class="col-sm-3">
					<input type="text" class="form-control input-lg" id="firstName"
						name="firstName" value="${newUser.firstName}">
				</div>
			</div>
			<div class="form-group form-group-lg ">
				<label for="lastName" class="control-label col-sm-2"> <fmt:message
						key="jsp.user.lastname" />
				</label>
				<div class="col-sm-3">
					<input type="text" class="form-control input-lg" id="lastName"
						name="lastName" value="${newUser.lastName}">
				</div>
			</div>
			<div class="form-group form-group-lg ">
				<label for="role" class="control-label col-sm-2"> <fmt:message
						key="jsp.user.role" />
				</label>
				<div class="col-sm-3">
					<label> <input type="radio" class="form-control input-lg"
						id="role" name="role" value="manager" />Manager
					</label> <label> <input type="radio"
						class="form-control" id="role" name="role" value="developer"
						checked="checked" />Developer
					</label>
				</div>
			</div>
			<div class="form-group ">
				<div class="col-sm-offset-2 col-sm-3">
					<input type="submit" class="btn btn-success btn-lg btn-block"
						value="<fmt:message key="jsp.register.user.save"/>" />
				</div>
			</div>
		</form>
	</div>

	<jsp:include page="/jsp/base/footer.jsp" />

</body>
</html>