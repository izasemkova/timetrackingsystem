<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li>
					<form name="languageForm" method="GET" action="controller">
						<c:choose>
							<c:when test="${user.role=='developer'}">
								<input type="hidden" name="command" value="developer_page" />
							</c:when>
							<c:when test="${user.role=='manager'}">
								<input type="hidden" name="command" value="manager_page" />
							</c:when>
							<c:otherwise>
								<input type="hidden" name="command" value="main_page" />
							</c:otherwise>
						</c:choose>

						<input class="btn btn-link" type="submit"
							value="<fmt:message key="jsp.header.home"/>" />
					</form>
				</li>
				<c:choose>
					<c:when test="${user.role=='developer'}">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><fmt:message
									key="jsp.header.developer.menu" /> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li>
									<form name="progectListForm" method="GET" action="controller">
										<input type="hidden" name="command" value="developer_page" />
										<input class="btn btn-link-inverse" type="submit"
											value="<fmt:message key="jsp.header.project.list"/>" />
									</form>
								</li>
							</ul></li>
					</c:when>
					<c:when test="${user.role=='manager'}">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><fmt:message
									key="jsp.header.manager.menu" /> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li>
									<form name="userListForm" method="POST" action="controller">
										<input type="hidden" name="command" value="users_list_page" />
										<input class="btn btn-link-inverse" type="submit"
											value="<fmt:message key="jsp.header.manager.user.list"/>" />
									</form>
								</li>
								<li>
									<form name="progectListForm" method="GET" action="controller">
										<input type="hidden" name="command" value="manager_page" /> <input
											class="btn btn-link-inverse" type="submit"
											value="<fmt:message key="jsp.header.project.list"/>" />
									</form>
								</li>
								<li>
									<form name="createProjectForm" method="POST"
										action="controller">
										<input type="hidden" name="command" value="create_project" />
										<input class="btn btn-link-inverse" type="submit"
											value="<fmt:message key="jsp.header.manager.create.project"/>" />
									</form>
								</li>
							</ul></li>
					</c:when>
					<c:otherwise>
						<li>
							<form name="userListForm" method="POST" action="controller">
								<input type="hidden" name="command" value="add_new_user" /> <input
									class="btn btn-link" type="submit"
									value="<fmt:message key="jsp.header.registration"/>" />
							</form>
						</li>
					</c:otherwise>
				</c:choose>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<form name="languageForm" method="POST" action="controller">
						<input type="hidden" name="command" value="change_language" /> <input
							type="hidden" name="language" value="ru" /> <input
							class="btn btn-link" type="submit"
							value="<fmt:message key="jsp.header.language.ru"/>" />
					</form>
				</li>
				<li>
					<form name="languageForm" method="POST" action="controller">
						<input type="hidden" name="command" value="change_language" /> <input
							type="hidden" name="language" value="en" /> <input
							class="btn btn-link" type="submit"
							value="<fmt:message key="jsp.header.language.en"/>" />
					</form>
				</li>
				<c:if test="${not empty user}">
					<li>
						<form name="logoutForm" method="POST" action="controller">
							<input type="hidden" name="command" value="logout" /> <input
								class="btn btn-link" type="submit"
								value="<fmt:message key="jsp.header.logout"/>" />
						</form>
					</li>
				</c:if>
			</ul>
		</div>
	</div>
</div>


