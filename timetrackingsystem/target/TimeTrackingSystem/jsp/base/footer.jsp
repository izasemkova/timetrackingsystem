<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="footer">
	<div class="container">
		<p class="text-muted">
			<fmt:message key="jsp.footer.info" />
		</p>
	</div>
</div>