<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB_INF/tld/taglib.tld" prefix="tg"%>

<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locale/locale" scope="application" />

<html lang="language">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><fmt:message key="jsp.manager.page.title" /></title>
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/bootstrap/css/sticky-footer-navbar.css"
	rel="stylesheet">

<SCRIPT type="text/javascript">
<!--
	function validateForm() {
		valid = true;

		if (document.addTaskForm.name.value == "") {
			alert("Пожалуйста заполните название задачи.");
			valid = false;
		}

		return valid;
	}
//-->
</SCRIPT>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>

	<jsp:include page="base/header.jsp" />

	<div class="container">
		<div class="page-header">
			<h1>
				<fmt:message key="jsp.add.task.greeting" />
			</h1>
		</div>

		<!-- вывод сообщения об ошибке -->
		<!-- ПОЛЬЗОВАТЕЛЬСКИЙ ТЭГ -->
		<tg:jspmsg message="${newTaskMsg}" />

		<form name="addTaskForm" method="POST" action="controller"
			class="form-horizontal" role="form" onsubmit="return validateForm();">
			<input type="hidden" name="command" value="save_task" />
			<div class="form-group form-group-lg ">
				<label for="name" class="control-label col-sm-2"> <fmt:message
						key="jsp.task.name" />
				</label>
				<div class="col-sm-3">
					<input type="text" class="form-control input-lg" id="name"
						name="name" value="${newTask.name}">
				</div>
			</div>
			<div class="form-group form-group-lg ">
				<label for="responsible" class="control-label col-sm-2"> <fmt:message
						key="jsp.task.responsible" />
				</label>
				<div class="col-sm-3">
					<input type="text" class="form-control input-lg" id="responsible"
						name="responsible" value="${newTask.responsible}">
				</div>
			</div>
			<div class="form-group ">
				<div class="col-sm-offset-2 col-sm-3">
					<input type="submit" class="btn btn-success btn-lg btn-block"
						value="<fmt:message key="jsp.add.task.save"/>" />
				</div>
			</div>
		</form>
	</div>

	<jsp:include page="/jsp/base/footer.jsp" />

</body>
</html>