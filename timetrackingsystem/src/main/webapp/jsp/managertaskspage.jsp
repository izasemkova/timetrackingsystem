<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB_INF/tld/taglib.tld" prefix="tg"%>

<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locale/locale" scope="application" />

<html lang="language">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><fmt:message key="jsp.manager.page.title" /></title>
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/bootstrap/css/sticky-footer-navbar.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>

	<jsp:include page="base/header.jsp" />

	<div class="container">
		<div class="page-header">
			<h1>
				<fmt:message key="jsp.tasks.page.greeting" />
				<c:out value="${project.name}" />
			</h1>
		</div>
		<p class="lead">
			<fmt:message key="jsp.tasks.page.text" />
			<c:out value="${project.responsible}" />
		</p>

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th><fmt:message key="jsp.task.name" /></th>
					<th><fmt:message key="jsp.task.responsible" /></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${tasksList}" var="task">
					<tr>
						<td><c:out value="${task.name}" /></td>
						<td><c:out value="${task.responsible}" /></td>
						<td><c:if test="${not empty task.recordsList}">
								<form name="getRecordsForm" method="POST" action="controller">
									<input type="hidden" name="command" value="records_list_page" />
									<input type="hidden" name="taskId" value="${task.id}" /> <input
										class="btn btn-link" type="submit"
										value="<fmt:message key="jsp.task.get.records" />" />
								</form>
							</c:if></td>
						<td>
							<form name="setResponsibleForm" method="POST" action="controller">
								<input type="hidden" name="command" value="update_task" />
								<input type="hidden" name="taskId" value="${task.id}" /> <input
									class="btn btn-link" type="submit"
									value="<fmt:message key="jsp.tasks.page.set.responsible" />" />
							</form>
						</td>
						<td>
							<form name="addRecordForm" method="POST" action="controller">
								<input type="hidden" name="command" value="create_record" /> <input
									type="hidden" name="taskId" value="${task.id}" /> <input
									class="btn btn-link" type="submit"
									value="<fmt:message key="jsp.tasks.page.add.record" />" />
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>

		</table>

		<form name="addTaskForm" method="POST" action="controller">
			<input type="hidden" name="command" value="create_task" /> <input
				type="hidden" name="projectId" value="${project.id}" /> <input class="btn-lg"
				type="submit"
				value="<fmt:message key="jsp.manager.page.add.task" />" />
		</form>
	</div>

	<jsp:include page="/jsp/base/footer.jsp" />
</body>