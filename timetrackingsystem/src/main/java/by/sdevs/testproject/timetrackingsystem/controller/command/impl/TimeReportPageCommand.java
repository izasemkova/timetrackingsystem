	package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

	import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;
	import javax.servlet.http.HttpSession;

	import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
	import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
	import by.sdevs.testproject.timetrackingsystem.entity.User;

	/**
	 * This command class forwards to the time's report page.
	 * 
	 * @version 1.0 03 May 2016
	 * @author Irina Zasemkova
	 */
	public class TimeReportPageCommand implements ICommand {

		/**
		 * This constant contains path to main page
		 */
		private static final String MAIN_PAGE = "/jsp/main.jsp";
		/**
		 * This constant contains path to the time's report page
		 */
		private static final String MANAGER_PAGE = "/jsp/timereportpage.jsp";

		private static final String USER = "user";
		private static final String MANAGER = "manager";

		private static final String QUERY_STRING = "queryString";
		/**
		 * This constant contains value of query string for changing language
		 */
		private static final String QUERY_STRING_VALUE = "controller?command=time_report_page";

		/**
		 * This method forwards to the time's report page. If the user isn't authorized
		 * or doesn't have the manager's permissions, it will return main page. 
		 */
		@Override
		public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {
			HttpSession session = request.getSession(false);

			if (session == null) {
				return MAIN_PAGE;
			}

			User user = (User) session.getAttribute(USER);

			if ((user == null) || !MANAGER.equals(user.getRole())) {
				return MAIN_PAGE;
			}

			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE);
			
			return MANAGER_PAGE;
		}

	}


