package by.sdevs.testproject.timetrackingsystem.dao;

import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.Project;

/**
 * This interface contains methods that bind the project entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface IProjectDAO {

	Project get(int projectId) throws DAOException;

	List<Project> getProjectsList() throws DAOException;

	boolean create(Project newProject) throws DAOException;

	boolean update(Project project, int id) throws DAOException;

}
