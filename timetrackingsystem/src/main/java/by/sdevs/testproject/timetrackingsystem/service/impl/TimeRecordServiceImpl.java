package by.sdevs.testproject.timetrackingsystem.service.impl;

import java.sql.Date;
import java.util.Collections;
import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.ITimeRecordDAO;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.dao.impl.TimeRecordDAOImpl;
import by.sdevs.testproject.timetrackingsystem.entity.SumTimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;
import by.sdevs.testproject.timetrackingsystem.service.ITimeRecordService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for project entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class TimeRecordServiceImpl implements ITimeRecordService {

	private final static ITimeRecordDAO timeRecordDao = new TimeRecordDAOImpl();

	/**
	 * This method creates task using given TimeRecord entity
	 * 
	 * @param newTimeRecord
	 *            entity for creating TimeRecord
	 * @return true if the timeRecord has been created, or false otherwise
	 * @throws ServiceException
	 */
	@Override
	public boolean createTimeRecord(TimeRecord newTimeRecord, int taskId) throws ServiceException {
		try {
			timeRecordDao.create(newTimeRecord, taskId);
			return true;
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}

	/**
	 * This method gets list of TimeRecords for given taskId
	 * 
	 * @param taskId
	 *            id of task
	 * @return list of timeRecords
	 * @throws ServiceException
	 */
	@Override
	public List<TimeRecord> getTimeRecordsList(int taskId) throws ServiceException {
		List<TimeRecord> timeRecordsList = Collections.emptyList();
		try {
			timeRecordsList = timeRecordDao.getTimeRecordsList(taskId);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return timeRecordsList;
	}

	/**
	 * This method gets timeRecord with given id
	 * 
	 * @param timeRecordId
	 *            id of required timeRecord
	 * @return required timeRecord if it exist
	 * @throws ServiceException
	 */
	@Override
	public TimeRecord getTimeRecord(int timeRecordId) throws ServiceException {
		TimeRecord timeRecord = null;
		try {
			timeRecord = timeRecordDao.get(timeRecordId);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return timeRecord;
	}

	/**
	 * This method gets list of TimeRecords for given date period
	 * 
	 * @param startDate
	 * @param endDate
	 * @return list of timeRecords
	 * @throws ServiceException
	 */
	@Override
	public List<TimeRecord> getTimeRecordsList(Date startDate, Date endDate) throws ServiceException {
		List<TimeRecord> timeRecordsList = Collections.emptyList();
		try {
			timeRecordsList = timeRecordDao.getTimeRecordsList(startDate, endDate);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return timeRecordsList;
	}

	/**
	 * This method gets taskId for timeRecord with given id
	 * 
	 * @param id
	 *            id of required timeRecord
	 * @return taskId if required timeRecord exists
	 * @throws ServiceException
	 */
	@Override
	public int getTaskId(int id) throws ServiceException {
		int taskId;
		try {
			taskId = timeRecordDao.getTaskId(id);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return taskId;
	}

	/**
	 * This method gets list of time that every developer spend for given date
	 * period
	 * 
	 * @param startDate
	 * @param endDate
	 * @return list of timeRecords
	 * @throws ServiceException
	 */
	@Override
	public List<SumTimeRecord> getTimeList(Date startDate, Date endDate) throws ServiceException {
		List<SumTimeRecord> sumTimeRecordsList = Collections.emptyList();
		try {
			sumTimeRecordsList = timeRecordDao.getTimeList(startDate, endDate);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return sumTimeRecordsList;
	}

}
