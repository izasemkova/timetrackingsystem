package by.sdevs.testproject.timetrackingsystem.service;

import java.util.List;

import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for task entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface ITaskService {

	boolean createTask(Task newTask, int projectId) throws ServiceException;
	
	boolean updateTask(Task task, int id) throws ServiceException;

	Task getTask(int taskId) throws ServiceException;

	List<Task> getTasksList(int projectId) throws ServiceException;

	int getProjectId(int taskId)throws ServiceException;

	

}
