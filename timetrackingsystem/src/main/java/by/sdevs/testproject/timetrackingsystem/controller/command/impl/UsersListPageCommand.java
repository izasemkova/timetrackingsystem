package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IUserService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.UserServiceImpl;

/**
 * This command class forwards to the manager's page with list of all users.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class UsersListPageCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the page with list of all users.
	 */
	private static final String MANAGER_PAGE = "/jsp/userslist.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String USERS_LIST = "usersList";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=users_list_page";

	private final static IUserService userService = new UserServiceImpl();

	/**
	 * This method forwards to the manager's page with list of all users. If the
	 * user isn't authorized or doesn't have the manager's permissions, it will
	 * return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}
		
		try {
			List<User> userList = userService.getUsersList();
			request.setAttribute(USERS_LIST, userList);
			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE);
		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		return MANAGER_PAGE;
	}

}
