package by.sdevs.testproject.timetrackingsystem.service;

import java.util.List;

import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for project entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface IProjectService {

	boolean createProject(Project newProject) throws ServiceException;

	boolean updateProject(Project project, int id) throws ServiceException;

	Project getProject(int projectId) throws ServiceException;

	List<Project> getProjectsList() throws ServiceException;

}