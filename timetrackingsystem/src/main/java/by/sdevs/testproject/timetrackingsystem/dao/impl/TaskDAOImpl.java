package by.sdevs.testproject.timetrackingsystem.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.sdevs.testproject.timetrackingsystem.dao.ITaskDAO;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.ConnectionPool;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.exception.ConnectionException;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.Task;

/**
 * This class contains methods that bind the Task entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class TaskDAOImpl implements ITaskDAO {

	/**
	 * This object obtains logger for this class
	 */
	private final static Logger logger = Logger.getLogger(TaskDAOImpl.class);

	private static final String GET_TASK_SQL = "SELECT DISTINCT * FROM TASK WHERE ID = ?";
	private static final String CREATE_TASK_SQL = "INSERT INTO TASK (NAME, RESPONSIBLE, PROJECTID) VALUES (?, ?, ?)";
	private static final String GET_TASKS_LIST_BY_PROJECT_SQL = "SELECT * FROM TASK WHERE PROJECTID = ?";
	private static final String UPDATE_TASK_SQL = "UPDATE TASK SET RESPONSIBLE = ? WHERE ID = ?";

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String RESPONSIBLE = "responsible";
	private static final String PROJECT_ID = "projectid";

	/**
	 * This method creates task in database using given Task entity and project
	 * Id
	 * 
	 * @param newTask
	 *            entity of creating Task
	 * @param projectId
	 *            id of project which contains given task
	 * @return true if the task has been created, or false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean create(Task newTask, int projectId) throws DAOException {
		boolean isCreated = false;

		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(CREATE_TASK_SQL);

			statement.setString(1, newTask.getName());
			statement.setString(2, newTask.getResponsible());
			statement.setInt(3, projectId);
			isCreated = (statement.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return isCreated;
	}

	/**
	 * This method update task in database using given Task entity
	 * 
	 * @param task
	 *            entity of creating Task
	 * @param id
	 *            task id
	 * @return true if the task has been created, or false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean update(Task task, int id) throws DAOException {
		boolean isUpdated = false;

		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(UPDATE_TASK_SQL);

			statement.setString(1, task.getResponsible());
			statement.setInt(2, id);
			isUpdated = (statement.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return isUpdated;
	}

	/**
	 * This method gets list of tasks from database for given project
	 * 
	 * @param projectId
	 *            id of project
	 * @return list of tasks
	 * @throws DAOException
	 */
	@Override
	public List<Task> getTasksList(int projectId) throws DAOException {
		List<Task> tasksList = new ArrayList<Task>();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TASKS_LIST_BY_PROJECT_SQL);
			statement.setInt(1, projectId);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt(ID);
				String taskName = resultSet.getString(NAME);
				String taskResponssible = resultSet.getString(RESPONSIBLE);

				Task task = new Task();
				task.setId(id);
				task.setName(taskName);
				task.setResponsible(taskResponssible);

				tasksList.add(task);
			}
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return tasksList;
	}

	/**
	 * This method gets task with given id from database
	 * 
	 * @param taskId
	 *            id of required task
	 * @return required task if it exist
	 * @throws DAOException
	 */
	@Override
	public Task get(int taskId) throws DAOException {
		Task task = new Task();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TASK_SQL);
			statement.setInt(1, taskId);
			resultSet = statement.executeQuery();

			resultSet.next();
			int id = resultSet.getInt(ID);
			String taskName = resultSet.getString(NAME);
			String taskResponssible = resultSet.getString(RESPONSIBLE);

			task.setId(id);
			task.setName(taskName);
			task.setResponsible(taskResponssible);

		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return task;
	}

	/**
	 * This method gets projectId for task with given id from database
	 * 
	 * @param taskId
	 *            id of required task
	 * @return required projectId if task exists
	 * @throws DAOException
	 */
	@Override
	public int getProjectId(int taskId) throws DAOException {
		int projectId;
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TASK_SQL);
			statement.setInt(1, taskId);
			resultSet = statement.executeQuery();

			resultSet.next();
			projectId = resultSet.getInt(PROJECT_ID);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return projectId;
	}

}
