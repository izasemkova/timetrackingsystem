package by.sdevs.testproject.timetrackingsystem.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.sdevs.testproject.timetrackingsystem.dao.IProjectDAO;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.ConnectionPool;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.exception.ConnectionException;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.Project;

/**
 * This class contains methods that bind the Project entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class ProjectDAOImpl implements IProjectDAO {

	/**
	 * This object obtains logger for this class
	 */
	private final static Logger logger = Logger.getLogger(ProjectDAOImpl.class);

	private static final String GET_PROJECT_SQL = "SELECT DISTINCT * FROM PROJECT WHERE ID = ?";
	private static final String CREATE_PROJECT_SQL = "INSERT INTO PROJECT (NAME, RESPONSIBLE) VALUES (?, ?)";
	private static final String GET_PROJECTS_LIST_SQL = "SELECT * FROM PROJECT";
	private static final String UPDATE_PROJECT_SQL = "UPDATE PROJECT SET RESPONSIBLE = ? WHERE ID = ?";

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String RESPONSIBLE = "responsible";

	/**
	 * This method creates project in database using given Project entity
	 * 
	 * @param newProject
	 *            entity of creating Project
	 * @return true if the project has been created, or false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean create(Project newProject) throws DAOException {
		boolean isCreated = false;

		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(CREATE_PROJECT_SQL);

			statement.setString(1, newProject.getName());
			statement.setString(2, newProject.getResponsible());
			isCreated = (statement.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return isCreated;
	}
	
	/**
	 * This method update project in database using given Project entity
	 * 
	 * @param project
	 *            entity of creating Project
	 * @param id
	 *            project id
	 * @return true if the project has been created, or false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean update(Project project, int id) throws DAOException {
		boolean isUpdated = false;

		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(UPDATE_PROJECT_SQL);

			statement.setString(1, project.getResponsible());
			statement.setInt(2, id);
			isUpdated = (statement.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return isUpdated;
	}

	/**
	 * This method gets list of projects from database
	 * 
	 * @return list of projects
	 * @throws DAOException
	 */
	@Override
	public List<Project> getProjectsList() throws DAOException {
		List<Project> projectsList = new ArrayList<Project>();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_PROJECTS_LIST_SQL);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt(ID);
				String projectName = resultSet.getString(NAME);
				String projectResponssible = resultSet.getString(RESPONSIBLE);

				Project project = new Project();
				project.setId(id);
				project.setName(projectName);
				project.setResponsible(projectResponssible);

				projectsList.add(project);
			}
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return projectsList;
	}

	/**
	 * This method gets project with given id from database
	 * 
	 * @param projectId
	 *            id of required project
	 * @return required project if it exist
	 * @throws DAOException
	 */
	@Override
	public Project get(int projectId) throws DAOException {
		Project project = new Project();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_PROJECT_SQL);
			statement.setInt(1, projectId);
			resultSet = statement.executeQuery();

			resultSet.next();
			int id = resultSet.getInt(ID);
			String projectName = resultSet.getString(NAME);
			String projectResponssible = resultSet.getString(RESPONSIBLE);

			project.setId(id);
			project.setName(projectName);
			project.setResponsible(projectResponssible);

		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return project;
	}

}
