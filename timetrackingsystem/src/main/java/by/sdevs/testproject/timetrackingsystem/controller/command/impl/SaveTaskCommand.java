package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;

/**
 * This command class saves new task and forwards to the manager's page with
 * list of all tasks.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class SaveTaskCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the page for adding new task
	 */
	private static final String ADD_NEW_TASK_PAGE = "/jsp/addtask.jsp";
	/**
	 * This constant contains path to the page with list of all tasks.
	 */
	private static final String MANAGER_PAGE = "/jsp/managertaskspage.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String PROJECT = "project";
	private static final String TASKS_LIST = "tasksList";
	private static final String PROJECT_ID = "projectId";

	private static final String NAME = "name";
	private static final String RESPONSIBLE = "responsible";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=manager_tasks_page&projectId=";

	private static final String NEW_TASK_MSG_KEY = "newTaskMsg";
	private static final String NEW_TASK_MSG_VALUE = "jsp.add.task.error";

	private final static ITaskService taskService = new TaskServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method saves new task and forwards to the manager's page with
	 * list of all tasks. If the entered information contains errors, it will
	 * return to the page for adding new task. If the user isn't authorized
	 * or doesn't have the manager's permissions, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		HttpSession session = request.getSession(false);
		
		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}

		String name = request.getParameter(NAME);
		String responsible = request.getParameter(RESPONSIBLE);
		
		int projectID = (Integer)session.getAttribute(PROJECT_ID);

		String newTaskMsg = "";

		if (!validationParameters(name)) {
			newTaskMsg = ResourceManager.INSTANCE.getString(NEW_TASK_MSG_VALUE);
			request.setAttribute(NEW_TASK_MSG_KEY, newTaskMsg);
			return ADD_NEW_TASK_PAGE;
		}

		try {
			Task newTask = new Task();
			newTask.setName(name);
			newTask.setResponsible(responsible);
			boolean isCreated = taskService.createTask(newTask, projectID);

			if (!isCreated) {
				newTaskMsg = ResourceManager.INSTANCE.getString(NEW_TASK_MSG_VALUE);
				request.setAttribute(NEW_TASK_MSG_KEY, newTaskMsg);
				return ADD_NEW_TASK_PAGE;
			}

			Project project = projectService.getProject(projectID);
			request.setAttribute(PROJECT, project);
			List<Task> tasksList = taskService.getTasksList(projectID);
			request.setAttribute(TASKS_LIST, tasksList);
			request.removeAttribute(NEW_TASK_MSG_KEY);
			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE + projectID);

		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}
		return MANAGER_PAGE;

	}

	private boolean validationParameters(String name) {
		if (name.isEmpty() || (name.length() > 250)) {
			return false;
		}
		return true;
	}
}
