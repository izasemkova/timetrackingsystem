package by.sdevs.testproject.timetrackingsystem.dao.exception;

/**
 * The DAO exception class.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class DAOException extends Exception {
	private static final long serialVersionUID = 6939510660194058253L;

	public DAOException(String message) {
		super(message);
	}
	
	public DAOException(String message, Exception e) {
		super(message, e);
	}
}
