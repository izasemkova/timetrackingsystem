package by.sdevs.testproject.timetrackingsystem.controller;

import java.util.HashMap;
import java.util.Map;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.MainPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.LoginCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.LogoutCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.ChangeLanguageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.ManagerPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.DeveloperPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.CreateProjectCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.SaveProjectCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.UpdateProjectCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.CreateTaskCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.SaveTaskCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.UpdateTaskCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.CreateRecordCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.SaveRecordCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.RecordsListPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.AddNewUserCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.SaveUserCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.UsersListPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.ManagerTasksPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.DeveloperTasksPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.SetProjectResponsibleCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.SetTaskResponsibleCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.RecordReportPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.TimeReportPageCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.CreateRecordReportCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.CreateTimeReportCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.DownloadRecordReportCommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.impl.DownloadTimeReportCommand;

/**
 * The controller helper class. It fills map of commands and returns suitable
 * command to controller.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public final class ControllerHelper {
	private Map<CommandName, ICommand> commands = new HashMap<CommandName, ICommand>();

	public ControllerHelper() {
		commands.put(CommandName.MAIN_PAGE, new MainPageCommand());
		commands.put(CommandName.LOGIN, new LoginCommand());
		commands.put(CommandName.LOGOUT, new LogoutCommand());
		commands.put(CommandName.CHANGE_LANGUAGE, new ChangeLanguageCommand());
		commands.put(CommandName.MANAGER_PAGE, new ManagerPageCommand());
		commands.put(CommandName.DEVELOPER_PAGE, new DeveloperPageCommand());
		commands.put(CommandName.ADD_NEW_USER, new AddNewUserCommand());
		commands.put(CommandName.SAVE_USER, new SaveUserCommand());
		commands.put(CommandName.USERS_LIST_PAGE, new UsersListPageCommand());
		commands.put(CommandName.CREATE_PROJECT, new CreateProjectCommand());
		commands.put(CommandName.SAVE_PROJECT, new SaveProjectCommand());
		commands.put(CommandName.UPDATE_PROJECT, new UpdateProjectCommand());
		commands.put(CommandName.SET_PROJECT_RESPONSIBLE, new SetProjectResponsibleCommand());
		commands.put(CommandName.CREATE_TASK, new CreateTaskCommand());
		commands.put(CommandName.SAVE_TASK, new SaveTaskCommand());
		commands.put(CommandName.UPDATE_TASK, new UpdateTaskCommand());
		commands.put(CommandName.SET_TASK_RESPONSIBLE, new SetTaskResponsibleCommand());
		commands.put(CommandName.CREATE_RECORD, new CreateRecordCommand());
		commands.put(CommandName.SAVE_RECORD, new SaveRecordCommand());
		commands.put(CommandName.RECORDS_LIST_PAGE, new RecordsListPageCommand());
		commands.put(CommandName.MANAGER_TASKS_PAGE, new ManagerTasksPageCommand());
		commands.put(CommandName.DEVELOPER_TASKS_PAGE, new DeveloperTasksPageCommand());
		commands.put(CommandName.RECORD_REPORT_PAGE, new RecordReportPageCommand());
		commands.put(CommandName.TIME_REPORT_PAGE, new TimeReportPageCommand());
		commands.put(CommandName.CREATE_RECORD_REPORT, new CreateRecordReportCommand());
		commands.put(CommandName.CREATE_TIME_REPORT, new CreateTimeReportCommand());
		commands.put(CommandName.DOWNLOAD_RECORD_REPORT, new DownloadRecordReportCommand());
		commands.put(CommandName.DOWNLOAD_TIME_REPORT, new DownloadTimeReportCommand());

	}

	public ICommand getCommand(String commandName) {
		CommandName name = CommandName.valueOf(commandName.toUpperCase());
		return commands.get(name);
	}

	enum CommandName {
		MAIN_PAGE, LOGIN, LOGOUT, CHANGE_LANGUAGE, MANAGER_PAGE, DEVELOPER_PAGE, ADD_NEW_USER, SAVE_USER, USERS_LIST_PAGE, CREATE_PROJECT, SAVE_PROJECT, UPDATE_PROJECT, SET_PROJECT_RESPONSIBLE, CREATE_TASK, SAVE_TASK, UPDATE_TASK, SET_TASK_RESPONSIBLE, CREATE_RECORD, SAVE_RECORD, RECORDS_LIST_PAGE, MANAGER_TASKS_PAGE, DEVELOPER_TASKS_PAGE, RECORD_REPORT_PAGE, CREATE_RECORD_REPORT, DOWNLOAD_RECORD_REPORT, TIME_REPORT_PAGE, CREATE_TIME_REPORT, DOWNLOAD_TIME_REPORT 
	}

}
