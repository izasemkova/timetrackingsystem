package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.service.IUserService;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.UserServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;

/**
 * This command class forwards to the manager's page or developer's page.
 * 
 * @version 1.0 17 December 2015
 * @author Irina Zasemkova
 */
public class LoginCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the manager's page
	 */
	private static final String MANAGER_PAGE = "/jsp/managerpage.jsp";
	/**
	 * This constant contains path to the developer's page
	 */
	private static final String DEVELOPER_PAGE = "/jsp/developerpage.jsp";

	private static final String LOGIN = "login";
	private static final String PASSWORD = "password";
	private static final String USER = "user";
	private static final String PROJECTS_LIST = "projectsList";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=";

	private static final String LOGIN_MSG_KEY = "loginmsg";
	private static final String EMPTY_LOGIN_MSG_VALUE = "jsp.main.page.empty.login";
	private static final String NO_USER_LOGIN_MSG_VALUE = "jsp.main.page.login.error";

	private static final String MANAGER = "manager";
	private static final String DEVELOPER = "developer";

	/**
	 * This constant contains value of command for query string for forwarding to main page 
	 */
	private static final String MAIN_PAGE_COMMAND = "main_page";
	/**
	 * This constant contains value of command for query string for forwarding to administrator's page 
	 */
	private static final String MANAGER_PAGE_COMMAND = "manager_page";
	/**
	 * This constant contains value of command for query string for forwarding to user's page 
	 */
	private static final String DEVELOPER_PAGE_COMMAND = "developer_page";

	private final static IUserService userService = new UserServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This command class forwards to the manager's page or developer's page if user is registered in system.
	 * If there is no user with such login and password, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {
		String login = request.getParameter(LOGIN);
		String password = request.getParameter(PASSWORD);

		String loginMsg = "";

		if (!validationParameters(login, password)) {
			loginMsg = ResourceManager.INSTANCE.getString(EMPTY_LOGIN_MSG_VALUE);
			request.setAttribute(LOGIN_MSG_KEY, loginMsg);
			return MAIN_PAGE;
		}

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}

		try {
			User user = userService.getUser(login, password);

			if (user == null) {
				loginMsg = ResourceManager.INSTANCE.getString(NO_USER_LOGIN_MSG_VALUE);
				request.setAttribute(LOGIN_MSG_KEY, loginMsg);
				return MAIN_PAGE;
			}

			session = request.getSession(true);
			session.setAttribute(USER, user);

			request.removeAttribute(LOGIN_MSG_KEY);
			
			List<Project> projectsList = projectService.getProjectsList();

			request.setAttribute(PROJECTS_LIST, projectsList);

			switch (user.getRole()) {
			case MANAGER:
				request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE + MANAGER_PAGE_COMMAND);

				return MANAGER_PAGE;
			case DEVELOPER:
				request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE + DEVELOPER_PAGE_COMMAND);

				return DEVELOPER_PAGE;
			default:
				request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE + MAIN_PAGE_COMMAND);

				return MAIN_PAGE;
			}

		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}
	}

	private boolean validationParameters(String login, String password) {
		if (!login.isEmpty() && !password.isEmpty()) {
			return true;
		}
		return false;
	}

}
