package by.sdevs.testproject.timetrackingsystem.dao.connectionpool;

import java.util.ResourceBundle;

/**
 * This class contains methods that get parameters of database connection from the file
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class DBResourceManager {
	
	/**
	 * This constant contains path to .properties file with parameters of database connection in resources folder
	 */
	private final static String DB_RESOURCE_PATH = "db";

	private static ResourceBundle bundle = ResourceBundle.getBundle(DB_RESOURCE_PATH);

	public String getValue(String key) {
		return bundle.getString(key);
	}

}
