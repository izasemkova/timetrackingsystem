package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;

/**
 * This command class forwards to the main page.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class MainPageCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=main_page";

	/**
	 * This command class forwards to the main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {
		request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE);

		return MAIN_PAGE;
	}
}
