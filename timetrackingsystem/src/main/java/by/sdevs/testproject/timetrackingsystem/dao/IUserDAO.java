package by.sdevs.testproject.timetrackingsystem.dao;

import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.User;

/**
 * This interface contains methods that bind the user entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface IUserDAO {
	
	User get(int userId) throws DAOException;
	
	User getUserByLoginPass(String login, String password) throws DAOException;

	boolean checkUser(String login) throws DAOException;

	List<User> getUsersList() throws DAOException;

	boolean create(User newUser) throws DAOException;

	void delete(int userId) throws DAOException;

}
