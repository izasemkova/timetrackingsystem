package by.sdevs.testproject.timetrackingsystem.controller.util;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * This class selects the way for sending response to client
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class ResponseManager {

	/**
	 * This object obtains logger for this class
	 */
	private final static Logger logger = Logger.getLogger(ResponseManager.class);

	/**
	 * This constant contains path to error page
	 */
	private static final String ERROR_PAGE = "/jsp/error.jsp";

	/**
	 * This method selects the way for sending response to client.
	 * 
	 * @param request
	 *            HttpServletRequest from the client.
	 * @param response
	 *            HttpServletResponse to the client.
	 * @param page
	 *            page where request is forwarded or redirected
	 * @throws ServletException
	 * @throws IOException
	 */
	public static void sendResponse(HttpServletRequest request, HttpServletResponse response, String page)
			throws ServletException, IOException {

		if (!response.isCommitted()) {
			if (page.contains("?")) {
				try {
					response.sendRedirect(page);
				} catch (IOException exception) {
					logger.debug("Unable to redirect. " + exception.getMessage());
				}
			} else {
				RequestDispatcher dispatcher = request.getRequestDispatcher(page);
				if (dispatcher != null) {
					try {
						dispatcher.forward(request, response);
					} catch (Exception e) {
						dispatcher = request.getRequestDispatcher(ERROR_PAGE);
						dispatcher.forward(request, response);
					}

				}
			}
		}
	}

}
