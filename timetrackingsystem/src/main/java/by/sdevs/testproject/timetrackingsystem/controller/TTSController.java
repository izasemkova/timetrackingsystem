package by.sdevs.testproject.timetrackingsystem.controller;

import java.io.IOException;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.sdevs.testproject.timetrackingsystem.controller.ControllerHelper;
import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResponseManager;

/**
 * The controller class. It takes request from client, calls suitable command
 * class and returns response to client.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class TTSController extends HttpServlet {
	private static final long serialVersionUID = 708758898977430808L;

	/**
	 * This object obtains logger for this class
	 */
	private final static Logger logger = Logger.getLogger(TTSController.class);

	private static final String REQUEST_COMMAND_PARAMETR = "command";

	private static final String DEFAULT_PAGE = "/jsp/main.jsp";

	private static final String INTERRUPTED_EXC_MSG = "Thread is interrupted during the activity.";

	ReadWriteLock rwlLock = new ReentrantReadWriteLock();

	private ControllerHelper helper = new ControllerHelper();

	public TTSController() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * This method takes request from client, calls suitable command class and
	 * returns response to client.
	 * 
	 * @param request
	 *            HttpServletRequest from the client.
	 * @param response
	 *            HttpServletResponse to the client.
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String commandValue = request.getParameter(REQUEST_COMMAND_PARAMETR);
		String page = null;

		try {
			ICommand command = helper.getCommand(commandValue);
			page = command.execute(request, response, rwlLock);
		} catch (CommandException e) {
			logger.debug("Incorrect command. " + e.getMessage());
		} catch (InterruptedException e) {
			logger.error(INTERRUPTED_EXC_MSG + e);
		} finally {
			if ((page == null)) {
				page = DEFAULT_PAGE;
			}
		}

		ResponseManager.sendResponse(request, response, page);
	}

}
