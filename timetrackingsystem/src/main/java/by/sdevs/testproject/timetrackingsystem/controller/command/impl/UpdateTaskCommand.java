package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;

/**
 * This command class forwards to the page for setting responsible person.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class UpdateTaskCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the update task page
	 */
	private static final String UPDATE_TASK_PAGE = "/jsp/updatetask.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String TASK_ID = "taskId";
	private static final String TASK = "task";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=update_task&taskId=";

	private final static ITaskService taskService = new TaskServiceImpl();
	/**
	 * This method forwards to the page for setting responsible person for task.
	 * If the user isn't authorized or doesn't have the manager's permissions,
	 * it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		int taskId = Integer.parseInt(request.getParameter(TASK_ID));

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		session.setAttribute(TASK_ID, taskId);

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}
		
		try {
			Task task = taskService.getTask(taskId);
			request.setAttribute(TASK, task);
		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE + taskId);

		return UPDATE_TASK_PAGE;
	}

}
