package by.sdevs.testproject.timetrackingsystem.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * This is user's tag class. This tag displays message on jsp page if it's not empty.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class DisplaingMessageTag extends TagSupport {
	private static final long serialVersionUID = 5111811968932120650L;
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut();
			if (!message.isEmpty()) {
				out.write("<div class=\"row\">");
				out.write("<div class=\"col-sm-offset-2 col-sm-3\">");
				out.write("<p class=\"text-danger text-center\">");
				out.write(message);
				out.write("</p>");
				out.write("</div>");
				out.write("</div>");
			}
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
