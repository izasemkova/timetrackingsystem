package by.sdevs.testproject.timetrackingsystem.dao;

import java.sql.Date;
import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.SumTimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;

/**
 * This interface contains methods that bind the timeRecord entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface ITimeRecordDAO {

	boolean create(TimeRecord newTimeRecord, int taskId) throws DAOException;

	List<TimeRecord> getTimeRecordsList(int taskId) throws DAOException;

	TimeRecord get(int timeRecordId) throws DAOException;

	List<TimeRecord> getTimeRecordsList(Date startDate, Date endDate) throws DAOException;

	int getTaskId(int id) throws DAOException;

	List<SumTimeRecord> getTimeList(Date startDate, Date endDate) throws DAOException;

}
