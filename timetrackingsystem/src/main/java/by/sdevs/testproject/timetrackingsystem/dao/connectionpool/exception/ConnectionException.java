package by.sdevs.testproject.timetrackingsystem.dao.connectionpool.exception;

/**
 * The connection exception class.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class ConnectionException extends Exception {
	private static final long serialVersionUID = 2544156581413411986L;

	public ConnectionException(String message) {
		super(message);
	}
	
	public ConnectionException(String message, Exception e) {
		super(message, e);
	}
	
}