package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;

/**
 * This command class forwards to the page for registering new user.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class AddNewUserCommand implements ICommand {

	/**
	 * This constant contains path to register page
	 */
	private static final String ADD_NEW_USER_PAGE = "/jsp/register.jsp";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=add_new_user";

	/**
	 * This method forwards to the page for registering new reader.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE);

		return ADD_NEW_USER_PAGE;
	}

}
