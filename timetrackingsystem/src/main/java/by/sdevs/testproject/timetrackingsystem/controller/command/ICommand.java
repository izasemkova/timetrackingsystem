package by.sdevs.testproject.timetrackingsystem.controller.command;

import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;

/**
 * The command interface.
 * 
 * @version 1.0 02 Мау 2016
 * @author Irina Zasemkova
 */
public interface ICommand {
	/**
	 * This method makes required actions that client wants to be done and
	 * returns path to page for response.
	 * 
	 * @param request
	 *            HttpServletRequest from the client.
	 * @param response
	 *            HttpServletResponse to the client.
	 * @return page where request is forwarded
	 * @throws CommandException
	 */
	String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException;
}
