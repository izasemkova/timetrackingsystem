package by.sdevs.testproject.timetrackingsystem.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.sdevs.testproject.timetrackingsystem.dao.IUserDAO;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.ConnectionPool;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.exception.ConnectionException;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.User;

/**
 * This class contains methods that bind the User entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class UserDAOImpl implements IUserDAO {

	/**
	 * This object obtains logger for this class
	 */
	private final static Logger logger = Logger.getLogger(UserDAOImpl.class);

	private static final String LOGIN_CHECK_SQL = "SELECT * FROM USER WHERE LOGIN = ?";
	private static final String GET_USER_BY_LOGIN_PASSWORD_SQL = "SELECT DISTINCT * FROM USER WHERE LOGIN = ? AND PASSWORD = PASSWORD(?)";
	private static final String GET_USER_SQL = "SELECT DISTINCT * FROM USER WHERE ID = ?";
	private static final String CREATE_USER_SQL = "INSERT INTO USER (LOGIN, PASSWORD, FIRSTNAME, LASTNAME, ROLE) VALUES (?, PASSWORD(?), ?, ?, ?)";
	private static final String GET_USERS_LIST_SQL = "SELECT * FROM USER";
	private static final String DELETE_USER_SQL = "DELETE FROM USER WHERE ID = ?";

	private static final String ID = "id";
	private static final String LOGIN = "login";
	private static final String PASSWORD = "password";
	private static final String FIRST_NAME = "firstname";
	private static final String LAST_NAME = "lastname";
	private static final String ROLE = "role";

	/**
	 * This method creates user in database using given User entity
	 * 
	 * @param newUser
	 *            entity of creating User
	 * @return true if the user has been created, or false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean create(User newUser) throws DAOException {
		boolean isCreated = false;

		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(CREATE_USER_SQL);

			statement.setString(1, newUser.getLogin());
			statement.setString(2, newUser.getPassword());
			statement.setString(3, newUser.getFirstName());
			statement.setString(4, newUser.getLastName());
			statement.setString(5, newUser.getRole());
			isCreated = (statement.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return isCreated;
	}

	public boolean checkUser(String login) throws DAOException {
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(LOGIN_CHECK_SQL);
			statement.setString(1, login);
			resultSet = statement.executeQuery();
			boolean checkResult = resultSet.next();

			return checkResult;
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
	}

	/**
	 * This method gets user with given login and password from database
	 * 
	 * @param login
	 *            user's login
	 * @param password
	 *            user's password
	 * @return required order if it exist
	 * @throws DAOException
	 */
	public User getUserByLoginPass(String login, String password) throws DAOException {
		User user = new User();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_USER_BY_LOGIN_PASSWORD_SQL);
			statement.setString(1, login);
			statement.setString(2, password);
			resultSet = statement.executeQuery();

			resultSet.next();
			int id = resultSet.getInt(ID);
			String userLogin = resultSet.getString(LOGIN);
			String userPassword = resultSet.getString(PASSWORD);
			String userFirstName = resultSet.getString(FIRST_NAME);
			String userLastName = resultSet.getString(LAST_NAME);
			String userRole = resultSet.getString(ROLE);

			user.setId(id);
			user.setLogin(userLogin);
			user.setPassword(userPassword);
			user.setFirstName(userFirstName);
			user.setLastName(userLastName);
			user.setRole(userRole);

		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return user;
	}

	/**
	 * This method gets list of users from database
	 * 
	 * @return list of users
	 * @throws DAOException
	 */
	@Override
	public List<User> getUsersList() throws DAOException {
		List<User> usersList = new ArrayList<User>();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_USERS_LIST_SQL);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt(ID);
				String userLogin = resultSet.getString(LOGIN);
				String userPassword = resultSet.getString(PASSWORD);
				String userFirstName = resultSet.getString(FIRST_NAME);
				String userLastName = resultSet.getString(LAST_NAME);
				String userRole = resultSet.getString(ROLE);

				User user = new User();
				user.setId(id);
				user.setLogin(userLogin);
				user.setPassword(userPassword);
				user.setFirstName(userFirstName);
				user.setLastName(userLastName);
				user.setRole(userRole);

				usersList.add(user);
			}
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return usersList;
	}

	/**
	 * This method gets user with given id from database
	 * 
	 * @param userId
	 *            id of required user
	 * @return required user if he exist
	 * @throws DAOException
	 */
	@Override
	public User get(int userId) throws DAOException {
		User user = new User();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_USER_SQL);
			statement.setInt(1, userId);
			resultSet = statement.executeQuery();

			resultSet.next();
			int id = resultSet.getInt(ID);
			String userLogin = resultSet.getString(LOGIN);
			String userPassword = resultSet.getString(PASSWORD);
			String userFirstName = resultSet.getString(FIRST_NAME);
			String userLastName = resultSet.getString(LAST_NAME);
			String userRole = resultSet.getString(ROLE);

			user.setId(id);
			user.setLogin(userLogin);
			user.setPassword(userPassword);
			user.setFirstName(userFirstName);
			user.setLastName(userLastName);
			user.setRole(userRole);

		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return user;
	}

	/**
	 * This method deletes user with given id from database
	 * 
	 * @param id
	 *            id of required user
	 * @throws DAOException
	 */
	@Override
	public void delete(int userId) throws DAOException {
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(DELETE_USER_SQL);
			statement.setInt(1, userId);
			statement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}

		}

	}

}
