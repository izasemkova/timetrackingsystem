package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.ITimeRecordService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TimeRecordServiceImpl;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * This command class creates record's report and forwards to the record's
 * report page.
 * 
 * @version 1.0 04 May 2016
 * @author Irina Zasemkova
 */
public class DownloadRecordReportCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the record's report page
	 */
	private static final String MANAGER_PAGE = "/jsp/recordreportpage.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String STARTDATE = "startdate";
	private static final String ENDDATE = "enddate";
	private static final String RECORDS_LIST = "recordsList";
	private static final String TASKS_LIST = "tasksList";
	private static final String PROJECTS_LIST = "projectsList";
	
	private static final String REPORT_GREETING = "jsp.records.repord.greeting";
	private static final String REPORT_GREETING_END = "jsp.records.repord.greeting.end";
	private static final String REPORT_DATE = "jsp.record.date";
	private static final String REPORT_LASTNAME = "jsp.user.lastname";
	private static final String REPORT_FIRSTNAME = "jsp.user.firstname";
	private static final String REPORT_HOURS = "jsp.record.hours";
	private static final String REPORT_TASK = "jsp.record.task";
	private static final String REPORT_PROJECT = "jsp.record.project";

	private final static ITimeRecordService timeRecordService = new TimeRecordServiceImpl();
	private final static ITaskService taskService = new TaskServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method forwards to the record's report page. If the user isn't
	 * authorized or doesn't have the manager's permissions, it will return main
	 * page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock)
			throws CommandException, InterruptedException {

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);
		Date startDate = (Date) session.getAttribute(STARTDATE); 
		Date endDate = (Date) session.getAttribute(ENDDATE); 

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}

		List<TimeRecord> timeRecordsList = new ArrayList<TimeRecord>();
		List<Task> tasksList = new ArrayList<Task>();
		List<Project> projectsList = new ArrayList<Project>();

		try {
			timeRecordsList = timeRecordService.getTimeRecordsList(startDate, endDate);
			request.setAttribute(RECORDS_LIST, timeRecordsList);

			for (TimeRecord timeRecord : timeRecordsList) {
				int taskId = timeRecordService.getTaskId(timeRecord.getId());
				Task task = taskService.getTask(taskId);
				tasksList.add(task);
				int projectId = taskService.getProjectId(taskId);
				Project project = projectService.getProject(projectId);
				projectsList.add(project);
			}
			request.setAttribute(TASKS_LIST, tasksList);
			request.setAttribute(PROJECTS_LIST, projectsList);
			request.setAttribute(STARTDATE, startDate);
			request.setAttribute(ENDDATE, endDate);

		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		String fileForRussAbsolWay = request.getScheme() + "://" + request.getServerName() + ":"
				+ request.getServerPort() + request.getContextPath() + "/forRussText/times.ttf";

		response.setContentType("application/pdf"); 
		response.setHeader("Content-Disposition", "attachment; filename = record_report_" + startDate + "-" + startDate + ".pdf"); 
		// файл сразу скачивается
		//response.setHeader("Content-Disposition", "filename = record_report_" + startDate + "-" + startDate + ".pdf"); 
		// файл загружается в окне браузера

		try {
			// step 1
			Document document = new Document(PageSize.A4, Utilities.millimetersToPoints(20),
					Utilities.millimetersToPoints(5), Utilities.millimetersToPoints(5),
					Utilities.millimetersToPoints(5));
			try {
				// step 2
				PdfWriter.getInstance(document, response.getOutputStream());
			} catch (IOException e) {
				throw new CommandException("IOException: " + e.getMessage(), e);
			}

			// step 3 - Открытие документа
			document.open();

			// step 4 Подготовка и add
			BaseFont times;
			try {
				times = BaseFont.createFont(fileForRussAbsolWay, "cp1251", BaseFont.EMBEDDED);
			} catch (IOException e) {
				throw new CommandException("IOException: " + e.getMessage(), e);
			} // ФАЙЛ
				// ДЛЯ
				// РУССИФИКАЦИИ
				// =
				// times.ttf

			Font font12 = new Font(times, 12);

			Font headerFont = new Font(times, 16);

			String header = ResourceManager.INSTANCE.getString(REPORT_GREETING) + startDate + ResourceManager.INSTANCE.getString(REPORT_GREETING_END) + endDate;

			Paragraph reportHeader = new Paragraph(15, header, headerFont);
			reportHeader.setAlignment(Element.ALIGN_CENTER);
			document.add(reportHeader);
			document.add(new Paragraph(" "));

			PdfPTable recordTable = createReportTable(font12, timeRecordsList, tasksList, projectsList);
			document.add(recordTable);
			document.add(new Paragraph(" "));

			document.close();

		} catch (DocumentException ex) {
			throw new CommandException("DocumentException: " + ex.getMessage(), ex);
		}

		return MANAGER_PAGE;
		
	}

	protected PdfPTable createReportTable(Font font12, List<TimeRecord> timeRecordsList, List<Task> tasksList,
			List<Project> projectsList) throws DocumentException {

		PdfPTable table = new PdfPTable(6);

		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_DATE), font12));
		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_FIRSTNAME), font12));
		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_LASTNAME), font12));
		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_HOURS), font12));
		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_TASK), font12));
		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_PROJECT), font12));
		if (timeRecordsList.size() > 0) {
			for (int i = 0; i < timeRecordsList.size(); i++) {
				table.addCell(new Paragraph(timeRecordsList.get(i).getDate().toString(), font12));
				table.addCell(new Paragraph(timeRecordsList.get(i).getFirstName(), font12));
				table.addCell(new Paragraph(timeRecordsList.get(i).getLastName(), font12));
				table.addCell(new Paragraph(String.valueOf(timeRecordsList.get(i).getHours()), font12));
				table.addCell(new Paragraph(tasksList.get(i).getName(), font12));
				table.addCell(new Paragraph(projectsList.get(i).getName(), font12));
			}
		}

		float[] columnWidths = { 100, 100, 100, 100, 100, 100 }; // Задаётся
																	// массив
																	// размеры
																	// колонок
		table.setWidths(columnWidths);// Задаётся размеры колонок

		return table;
	}

}
