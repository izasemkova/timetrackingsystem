package by.sdevs.testproject.timetrackingsystem.entity;

import java.io.Serializable;
import java.util.List;

/**
 * This class is entity that defines the task's parameters
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class Task implements Serializable {
	private static final long serialVersionUID = 999934596873208953L;
	
	private int id;
	private String name;
	private String responsible;
	private List<TimeRecord> recordsList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public List<TimeRecord> getRecordsList() {
		return recordsList;
	}

	public void setRecordsList(List<TimeRecord> recordsList) {
		this.recordsList = recordsList;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", name=" + name + ", responsible=" + responsible + ", recordsList=" + recordsList
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((recordsList == null) ? 0 : recordsList.hashCode());
		result = prime * result + ((responsible == null) ? 0 : responsible.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (recordsList == null) {
			if (other.recordsList != null)
				return false;
		} else if (!recordsList.equals(other.recordsList))
			return false;
		if (responsible == null) {
			if (other.responsible != null)
				return false;
		} else if (!responsible.equals(other.responsible))
			return false;
		return true;
	}

}
