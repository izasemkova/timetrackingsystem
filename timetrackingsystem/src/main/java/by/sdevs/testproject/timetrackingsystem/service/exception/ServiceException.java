package by.sdevs.testproject.timetrackingsystem.service.exception;

/**
 * The service exception class.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class ServiceException extends Exception {
	private static final long serialVersionUID = 1411891088113667648L;

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(String message, Exception e) {
		super(message, e);
	}
}

