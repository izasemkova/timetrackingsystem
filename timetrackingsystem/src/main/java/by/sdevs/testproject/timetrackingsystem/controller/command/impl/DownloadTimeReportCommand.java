package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.sql.Date;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.SumTimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.ITimeRecordService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.TimeRecordServiceImpl;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * This command class creates time's report and forwards to the time's report
 * page.
 * 
 * @version 1.0 04 May 2016
 * @author Irina Zasemkova
 */
public class DownloadTimeReportCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the time's report page
	 */
	private static final String MANAGER_PAGE = "/jsp/timereportpage.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String STARTDATE = "startdate";
	private static final String ENDDATE = "enddate";
	private static final String RECORDS_LIST = "recordsList";

	private static final String REPORT_GREETING = "jsp.records.repord.greeting";
	private static final String REPORT_GREETING_END = "jsp.records.repord.greeting.end";
	private static final String REPORT_LASTNAME = "jsp.user.lastname";
	private static final String REPORT_FIRSTNAME = "jsp.user.firstname";
	private static final String REPORT_HOURS = "jsp.record.hours";

	private final static ITimeRecordService timeRecordService = new TimeRecordServiceImpl();

	/**
	 * This method forwards to the time's report page. If the user isn't
	 * authorized or doesn't have the manager's permissions, it will return main
	 * page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock)
			throws CommandException, InterruptedException {

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);
		Date startDate = (Date) session.getAttribute(STARTDATE);
		Date endDate = (Date) session.getAttribute(ENDDATE);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}

		List<SumTimeRecord> timeRecordsList = new ArrayList<SumTimeRecord>();
		try {
			timeRecordsList = timeRecordService.getTimeList(startDate, endDate);
			request.setAttribute(RECORDS_LIST, timeRecordsList);
			request.setAttribute(STARTDATE, startDate);
			request.setAttribute(ENDDATE, endDate);
		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		String fileForRussAbsolWay = request.getScheme() + "://" + request.getServerName() + ":"
				+ request.getServerPort() + request.getContextPath() + "/forRussText/times.ttf";

		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition",
				"attachment; filename = record_report_" + startDate + "-" + startDate + ".pdf");
		// файл сразу скачивается
		// response.setHeader("Content-Disposition", "filename = record_report_"
		// + startDate + "-" + startDate + ".pdf");
		// файл загружается в окне браузера

		try {
			Document document = new Document(PageSize.A4, Utilities.millimetersToPoints(20),
					Utilities.millimetersToPoints(5), Utilities.millimetersToPoints(5),
					Utilities.millimetersToPoints(5));
			try {
				PdfWriter.getInstance(document, response.getOutputStream());
			} catch (IOException e) {
				throw new CommandException("IOException: " + e.getMessage(), e);
			}
			document.open();

			BaseFont times;
			try {
				times = BaseFont.createFont(fileForRussAbsolWay, "cp1251", BaseFont.EMBEDDED);
			} catch (IOException e) {
				throw new CommandException("IOException: " + e.getMessage(), e);
			}

			Font font12 = new Font(times, 12);

			Font headerFont = new Font(times, 16);

			String header = ResourceManager.INSTANCE.getString(REPORT_GREETING) + startDate
					+ ResourceManager.INSTANCE.getString(REPORT_GREETING_END) + endDate;

			Paragraph reportHeader = new Paragraph(15, header, headerFont);
			reportHeader.setAlignment(Element.ALIGN_CENTER);
			document.add(reportHeader);
			document.add(new Paragraph(" "));

			PdfPTable recordTable = createReportTable(font12, timeRecordsList);
			document.add(recordTable);
			document.add(new Paragraph(" "));

			document.close();

		} catch (DocumentException ex) {
			throw new CommandException("DocumentException: " + ex.getMessage(), ex);
		}

		return MANAGER_PAGE;

	}

	protected PdfPTable createReportTable(Font font12, List<SumTimeRecord> timeRecordsList) throws DocumentException {

		PdfPTable table = new PdfPTable(3);

		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_FIRSTNAME), font12));
		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_LASTNAME), font12));
		table.addCell(new Paragraph(ResourceManager.INSTANCE.getString(REPORT_HOURS), font12));

		if (timeRecordsList.size() > 0) {
			for (SumTimeRecord timeRecord : timeRecordsList) {
				table.addCell(new Paragraph(timeRecord.getFirstName(), font12));
				table.addCell(new Paragraph(timeRecord.getLastName(), font12));
				table.addCell(new Paragraph(String.valueOf(timeRecord.getSumHours()), font12));
			}
		}

		float[] columnWidths = { 200, 200, 100 };
		table.setWidths(columnWidths);

		return table;
	}

}
