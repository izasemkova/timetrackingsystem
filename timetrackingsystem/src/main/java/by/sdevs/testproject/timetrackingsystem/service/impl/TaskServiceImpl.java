package by.sdevs.testproject.timetrackingsystem.service.impl;

import java.util.Collections;
import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.ITaskDAO;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.dao.impl.TaskDAOImpl;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.ITimeRecordService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for project entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class TaskServiceImpl implements ITaskService {

	private final static ITaskDAO taskDao = new TaskDAOImpl();

	private final static ITimeRecordService timeRecordService = new TimeRecordServiceImpl();

	/**
	 * This method creates task using given Task entity
	 * 
	 * @param newTask
	 *            entity for creating Task
	 * @return true if the task has been created, or false otherwise
	 * @throws ServiceException
	 */
	@Override
	public boolean createTask(Task newTask, int projectId) throws ServiceException {
		try {
			taskDao.create(newTask, projectId);
			return true;
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}
	
	@Override
	public boolean updateTask(Task task, int id) throws ServiceException {
		try {
			taskDao.update(task, id);
			return true;
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}

	/**
	 * This method gets list of tasks for given projectId
	 * 
	 * @param projectId
	 *            id of project
	 * @return list of tasks
	 * @throws ServiceException
	 */
	@Override
	public List<Task> getTasksList(int projectId) throws ServiceException {
		List<Task> tasksList = Collections.emptyList();
		try {
			tasksList = taskDao.getTasksList(projectId);
			List<TimeRecord> timeRecordsList = Collections.emptyList();
			for (Task task : tasksList) {
				timeRecordsList = timeRecordService.getTimeRecordsList(task.getId());
				task.setRecordsList(timeRecordsList);
			}
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return tasksList;
	}

	/**
	 * This method gets task with given id
	 * 
	 * @param taskId
	 *            id of required task
	 * @return required task if it exist
	 * @throws ServiceException
	 */
	@Override
	public Task getTask(int taskId) throws ServiceException {
		Task task = null;
		try {
			task = taskDao.get(taskId);
			List<TimeRecord> timeRecordsList = timeRecordService.getTimeRecordsList(taskId);
			task.setRecordsList(timeRecordsList);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return task;
	}

	/**
	 * This method gets projectId for task with given id
	 * 
	 * @param taskId
	 *            id of required task
	 * @return projectId if required task exists
	 * @throws ServiceException
	 */
	@Override
	public int getProjectId(int taskId) throws ServiceException {
		int projectId;
		try {
			projectId = taskDao.getProjectId(taskId);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return projectId;
	}

	

}
