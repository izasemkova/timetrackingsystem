package by.sdevs.testproject.timetrackingsystem.controller.command.exception;

/**
 * The command exception class.
 * 
 * @version 1.0 02 Мау 2016
 * @author Irina Zasemkova
 */
public class CommandException extends Exception {
	private static final long serialVersionUID = 6243760796682802278L;

	public CommandException(String message) {
		super(message);
	}
	
	public CommandException(String message, Exception e) {
		super(message, e);
	}
}