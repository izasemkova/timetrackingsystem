package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;

/**
 * This command class sets project's responsible and forwards to the manager's page with
 * list of all projects.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class SetProjectResponsibleCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the page for updating project
	 */
	private static final String UPDATE_PROJECT_PAGE = "/jsp/updateproject.jsp";
	/**
	 * This constant contains path to the page with list of all projects.
	 */
	private static final String MANAGER_PAGE = "/jsp/managerpage.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String PROJECT_ID = "projectId";
	private static final String PROJECTS_LIST = "projectsList";

	private static final String RESPONSIBLE = "responsible";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=manager_page";

	private static final String UPDATED_PROJECT_MSG_KEY = "updatedProjectMsg";
	private static final String UPDATED_PROJECT_MSG_VALUE = "jsp.update.project.error";

	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method saves updated project and forwards to the manager's page with
	 * list of all projects. If the entered information contains errors, it will
	 * return to the page for adding new project. If the user isn't authorized
	 * or doesn't have the manager's permissions, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		HttpSession session = request.getSession(false);
		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}

		int projectId = (Integer) session.getAttribute(PROJECT_ID);

		String responsible = request.getParameter(RESPONSIBLE);

		String updateProjectMsg = "";

		if (!validationParameters(responsible)) {
			updateProjectMsg = ResourceManager.INSTANCE.getString(UPDATED_PROJECT_MSG_VALUE);
			request.setAttribute(UPDATED_PROJECT_MSG_KEY, updateProjectMsg);
			return UPDATE_PROJECT_PAGE;
		}
		
		rwlLock.writeLock().lock();
		
		try {
			Project project = projectService.getProject(projectId);
			project.setResponsible(responsible);
			boolean isUpdated = projectService.updateProject(project, projectId);

			if (!isUpdated) {
				updateProjectMsg = ResourceManager.INSTANCE.getString(UPDATED_PROJECT_MSG_VALUE);
				request.setAttribute(UPDATED_PROJECT_MSG_KEY, updateProjectMsg);
				return UPDATE_PROJECT_PAGE;
			}

			List<Project> projectsList = projectService.getProjectsList();
			request.setAttribute(PROJECTS_LIST, projectsList);
			request.removeAttribute(UPDATED_PROJECT_MSG_KEY);
			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE);

		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}
		
		rwlLock.writeLock().unlock();
		
		return MANAGER_PAGE;

	}

	private boolean validationParameters(String responsible) {
		if (responsible.isEmpty() || (responsible.length() > 60)) {
			return false;
		}
		return true;
	}
}
