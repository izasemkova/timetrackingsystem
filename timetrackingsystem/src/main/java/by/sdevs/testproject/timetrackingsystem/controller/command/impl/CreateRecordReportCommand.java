package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.ITimeRecordService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TimeRecordServiceImpl;

/**
 * This command class creates record's report and forwards to the record's
 * report page.
 * 
 * @version 1.0 04 May 2016
 * @author Irina Zasemkova
 */
public class CreateRecordReportCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the record's report page
	 */
	private static final String MANAGER_PAGE = "/jsp/recordreportpage.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String STARTDATE = "startdate";
	private static final String ENDDATE = "enddate";
	private static final String RECORDS_LIST = "recordsList";
	private static final String TASKS_LIST = "tasksList";
	private static final String PROJECTS_LIST = "projectsList";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=create_record_report";
	private static final String QUERY_STRING_STARTDATE_VALUE = "&startdate=";
	private static final String QUERY_STRING_ENDDATE_VALUE = "&enddate=";

	private static final String REPORT_MSG_KEY = "reportMsg";
	private static final String REPORT_MSG_VALUE = "jsp.report.error";

	private final static ITimeRecordService timeRecordService = new TimeRecordServiceImpl();
	private final static ITaskService taskService = new TaskServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method forwards to the record's report page. If the user isn't
	 * authorized or doesn't have the manager's permissions, it will return main
	 * page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock)
			throws CommandException, InterruptedException {

		Date startDate = null;
		Date endDate = null;
		
		String reportMsg = "";

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date parsed = format.parse(request.getParameter(STARTDATE));
			startDate = new Date(parsed.getTime());
			parsed = format.parse(request.getParameter(ENDDATE));
			endDate = new Date(parsed.getTime());
		} catch (ParseException e1) {
			reportMsg = ResourceManager.INSTANCE.getString(REPORT_MSG_VALUE);
			request.setAttribute(REPORT_MSG_KEY, reportMsg);
			return MANAGER_PAGE;
		}

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}

		if ((startDate == null) || (endDate == null)) {
			reportMsg = ResourceManager.INSTANCE.getString(REPORT_MSG_VALUE);
			request.setAttribute(REPORT_MSG_KEY, reportMsg);
			return MANAGER_PAGE;
		}
		
		try {
			List<TimeRecord> timeRecordsList = timeRecordService.getTimeRecordsList(startDate, endDate);
			request.setAttribute(RECORDS_LIST, timeRecordsList);

			List<Task> tasksList = new ArrayList<Task>();
			List<Project> projectsList = new ArrayList<Project>();

			for (TimeRecord timeRecord : timeRecordsList) {
				int taskId = timeRecordService.getTaskId(timeRecord.getId());
				Task task = taskService.getTask(taskId);
				tasksList.add(task);
				int projectId = taskService.getProjectId(taskId);
				Project project = projectService.getProject(projectId);
				projectsList.add(project);
			}
			request.setAttribute(TASKS_LIST, tasksList);
			request.setAttribute(PROJECTS_LIST, projectsList);
			request.setAttribute(STARTDATE, startDate);
			request.setAttribute(ENDDATE, endDate);
			
			session.setAttribute(STARTDATE, startDate);
			session.setAttribute(ENDDATE, endDate);
			
			request.removeAttribute(REPORT_MSG_KEY);

		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		session.setAttribute(QUERY_STRING,
				QUERY_STRING_VALUE + QUERY_STRING_STARTDATE_VALUE + startDate + QUERY_STRING_ENDDATE_VALUE + endDate);

		return MANAGER_PAGE;
	}

}
