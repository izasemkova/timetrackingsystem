package by.sdevs.testproject.timetrackingsystem.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.sdevs.testproject.timetrackingsystem.dao.ITimeRecordDAO;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.ConnectionPool;
import by.sdevs.testproject.timetrackingsystem.dao.connectionpool.exception.ConnectionException;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.SumTimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;

/**
 * This class contains methods that bind the TimeRecord entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class TimeRecordDAOImpl implements ITimeRecordDAO {

	/**
	 * This object obtains logger for this class
	 */
	private final static Logger logger = Logger.getLogger(TimeRecordDAOImpl.class);

	private static final String GET_TIME_RECORD_SQL = "SELECT DISTINCT * FROM TIMERECORD WHERE ID = ?";
	private static final String CREATE_TIME_RECORD_SQL = "INSERT INTO TIMERECORD (FIRSTNAME, LASTNAME, DATE, HOURS, TASKID) VALUES (?, ?, ?, ?, ?)";
	private static final String GET_TIME_RECORDS_LIST_BY_TASK_SQL = "SELECT * FROM TIMERECORD WHERE TASKID = ?";
	private static final String GET_TIME_RECORDS_LIST_BY_PERIOD_SQL = "SELECT * FROM TIMERECORD WHERE DATE BETWEEN ? AND ?";
	private static final String GET_TIME_LIST_BY_PERIOD_SQL = "SELECT FIRSTNAME, LASTNAME, SUM(HOURS) AS SUM_HOURS FROM TIMERECORD WHERE DATE BETWEEN ? AND ? GROUP BY FIRSTNAME, LASTNAME ;";

	private static final String ID = "id";
	private static final String FIRSTNAME = "firstname";
	private static final String LASTNAME = "lastname";
	private static final String DATE = "date";
	private static final String HOURS = "hours";
	private static final String SUM_HOURS = "SUM_HOURS";
	private static final String TASK_ID = "taskid";

	/**
	 * This method creates timeRecord in database using given TimeRecord entity
	 * and task Id
	 * 
	 * @param newTimeRecord
	 *            entity of creating TimeRecord
	 * @param taskId
	 *            id of task which contains given timeRecord
	 * @return true if the timeRecord has been created, or false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean create(TimeRecord newTimeRecord, int taskId) throws DAOException {
		boolean isCreated = false;

		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(CREATE_TIME_RECORD_SQL);

			statement.setString(1, newTimeRecord.getFirstName());
			statement.setString(2, newTimeRecord.getLastName());
			statement.setDate(3, newTimeRecord.getDate());
			statement.setInt(4, newTimeRecord.getHours());
			statement.setInt(5, taskId);
			isCreated = (statement.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return isCreated;
	}

	/**
	 * This method gets list of timeRecords from database for given task
	 * 
	 * @param taskId
	 *            id of task
	 * @return list of timeRecords
	 * @throws DAOException
	 */
	@Override
	public List<TimeRecord> getTimeRecordsList(int taskId) throws DAOException {
		List<TimeRecord> timeRecordList = new ArrayList<TimeRecord>();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TIME_RECORDS_LIST_BY_TASK_SQL);
			statement.setInt(1, taskId);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt(ID);
				String timeRecordFirstName = resultSet.getString(FIRSTNAME);
				String timeRecordLastName = resultSet.getString(LASTNAME);
				Date timeRecordDate = resultSet.getDate(DATE);
				int timeRecordHours = resultSet.getInt(HOURS);

				TimeRecord timeRecord = new TimeRecord();
				timeRecord.setId(id);
				timeRecord.setFirstName(timeRecordFirstName);
				timeRecord.setLastName(timeRecordLastName);
				timeRecord.setDate(timeRecordDate);
				timeRecord.setHours(timeRecordHours);

				timeRecordList.add(timeRecord);
			}
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return timeRecordList;
	}

	/**
	 * This method gets timeRecord with given id from database
	 * 
	 * @param timeRecordId
	 *            id of required timeRecord
	 * @return required timeRecord if it exist
	 * @throws DAOException
	 */
	@Override
	public TimeRecord get(int timeRecordId) throws DAOException {
		TimeRecord timeRecord = new TimeRecord();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TIME_RECORD_SQL);
			statement.setInt(1, timeRecordId);
			resultSet = statement.executeQuery();

			resultSet.next();
			int id = resultSet.getInt(ID);
			String timeRecordFirstName = resultSet.getString(FIRSTNAME);
			String timeRecordLastName = resultSet.getString(LASTNAME);
			Date timeRecordDate = resultSet.getDate(DATE);
			int timeRecordHours = resultSet.getInt(HOURS);

			timeRecord.setId(id);
			timeRecord.setFirstName(timeRecordFirstName);
			timeRecord.setLastName(timeRecordLastName);
			timeRecord.setDate(timeRecordDate);
			timeRecord.setHours(timeRecordHours);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return timeRecord;
	}

	/**
	 * This method gets list of timeRecords from database for given date period
	 * 
	 * @param startDate
	 * @param endDate
	 * @return list of timeRecords
	 * @throws DAOException
	 */
	@Override
	public List<TimeRecord> getTimeRecordsList(Date startDate, Date endDate) throws DAOException {
		List<TimeRecord> timeRecordList = new ArrayList<TimeRecord>();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TIME_RECORDS_LIST_BY_PERIOD_SQL);
			statement.setDate(1, startDate);
			statement.setDate(2, endDate);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt(ID);
				String timeRecordFirstName = resultSet.getString(FIRSTNAME);
				String timeRecordLastName = resultSet.getString(LASTNAME);
				Date timeRecordDate = resultSet.getDate(DATE);
				int timeRecordHours = resultSet.getInt(HOURS);

				TimeRecord timeRecord = new TimeRecord();
				timeRecord.setId(id);
				timeRecord.setFirstName(timeRecordFirstName);
				timeRecord.setLastName(timeRecordLastName);
				timeRecord.setDate(timeRecordDate);
				timeRecord.setHours(timeRecordHours);

				timeRecordList.add(timeRecord);
			}
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return timeRecordList;
	}

	/**
	 * This method gets taskId for timeRecord with given id from database
	 * 
	 * @param id
	 *            id of required timeRecords
	 * @return required taskId if timeRecords exists
	 * @throws DAOException
	 */
	@Override
	public int getTaskId(int id) throws DAOException {
		int taskId;
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TIME_RECORD_SQL);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			resultSet.next();
			taskId = resultSet.getInt(TASK_ID);
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return taskId;
	}

	/**
	 * This method gets list of time that every developer spend for given date
	 * period
	 * 
	 * @param startDate
	 * @param endDate
	 * @return list of timeRecords
	 * @throws DAOException
	 */
	@Override
	public List<SumTimeRecord> getTimeList(Date startDate, Date endDate) throws DAOException {
		List<SumTimeRecord> sumTimeRecordList = new ArrayList<SumTimeRecord>();
		ConnectionPool pool = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			pool = ConnectionPool.getInstance();
			connection = pool.takeConnection();
			statement = connection.prepareStatement(GET_TIME_LIST_BY_PERIOD_SQL);
			statement.setDate(1, startDate);
			statement.setDate(2, endDate);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				String timeRecordFirstName = resultSet.getString(FIRSTNAME);
				String timeRecordLastName = resultSet.getString(LASTNAME);
				int timeRecordHours = resultSet.getInt(SUM_HOURS);

				SumTimeRecord sumTimeRecord = new SumTimeRecord();
				sumTimeRecord.setFirstName(timeRecordFirstName);
				sumTimeRecord.setLastName(timeRecordLastName);
				sumTimeRecord.setSumHours(timeRecordHours);

				sumTimeRecordList.add(sumTimeRecord);
			}
		} catch (SQLException e) {
			throw new DAOException("SQL exception: " + e.getMessage(), e);
		} catch (ConnectionException e) {
			throw new DAOException("Connection exception: " + e.getMessage(), e);
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			} catch (SQLException e) {
				logger.debug("SQL exception: " + e.getMessage());
			}
		}
		return sumTimeRecordList;
	}

}
