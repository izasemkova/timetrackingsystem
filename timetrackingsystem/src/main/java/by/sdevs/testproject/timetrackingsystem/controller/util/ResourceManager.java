package by.sdevs.testproject.timetrackingsystem.controller.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This enum class uses for application internationalization
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public enum ResourceManager {
	INSTANCE;
	private ResourceBundle resourceBundle;
	/** This constant contains path to internationalization file  in resources folder */
	private final static String RESOURSE_NAME = "locale.locale";

	private ResourceManager() {
		resourceBundle = ResourceBundle.getBundle(RESOURSE_NAME, Locale.getDefault());
	}

	public void changeResource(Locale locale) {
		resourceBundle = ResourceBundle.getBundle(RESOURSE_NAME, locale);
	}

	public String getString(String key) {
		return resourceBundle.getString(key);
	}
}
