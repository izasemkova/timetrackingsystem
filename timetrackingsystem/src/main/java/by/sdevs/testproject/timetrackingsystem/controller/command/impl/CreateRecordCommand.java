package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.entity.User;

/**
 * This command class forwards to the page for adding new record for given task.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class CreateRecordCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the page for adding new task
	 */
	private static final String ADD_NEW_RECORD_PAGE = "/jsp/addrecord.jsp";

	private static final String USER = "user";
	private static final String TASK_ID = "taskId";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=create_record&taskId=";

	/**
	 * This method forwards to the page for adding new record. If the user isn't
	 * authorized, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {


		int taskId = Integer.parseInt(request.getParameter(TASK_ID));

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}
		session.setAttribute(TASK_ID, taskId);

		User user = (User) session.getAttribute(USER);

		if (user == null) {
			return MAIN_PAGE;
		}

		request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE + taskId);

		return ADD_NEW_RECORD_PAGE;
	}

}
