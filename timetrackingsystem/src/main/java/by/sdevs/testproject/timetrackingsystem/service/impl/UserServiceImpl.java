package by.sdevs.testproject.timetrackingsystem.service.impl;

import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.IUserDAO;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.dao.impl.UserDAOImpl;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IUserService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for user entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class UserServiceImpl implements IUserService {

	private final static IUserDAO userDao = new UserDAOImpl();

	/**
	 * This method gets user with given login and password
	 * 
	 * @param login
	 *            user's login
	 * @param password
	 *            user's password
	 * @return required user if it exist
	 * @throws ServiceException
	 */
	@Override
	public User getUser(String login, String password) throws ServiceException {
		User user = null;
		try {
			user = userDao.getUserByLoginPass(login, password);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return user;
	}

	/**
	 * This method creates user using given User entity
	 * 
	 * @param newUser
	 *            entity for creating User
	 * @return true if the user has been created, or false otherwise
	 * @throws ServiceException
	 */
	@Override
	public boolean createUser(User newUser) throws ServiceException {
		try {
			userDao.create(newUser);
			return true;
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}

	/**
	 * This method checks if the user with given login exists
	 * 
	 * @param login
	 *            user's login
	 * @return true if he exists, or false otherwise
	 * @throws ServiceException
	 */
	@Override
	public boolean checkUser(String login) throws ServiceException {
		try {
			boolean isExist = userDao.checkUser(login);
			return isExist;
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}

	/**
	 * This method gets list of users
	 * 
	 * @return list of users
	 * @throws ServiceException
	 */
	@Override
	public List<User> getUsersList() throws ServiceException {
		List<User> usersList;
		try {
			usersList = userDao.getUsersList();
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return usersList;
	}

	/**
	 * This method gets user with given id
	 * 
	 * @param userId
	 *            id of required user
	 * @return required user if he exist
	 * @throws ServiceException
	 */
	@Override
	public User getUser(int userId) throws ServiceException {
		User user = null;
		try {
			user = userDao.get(userId);
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return user;
	}

	/**
	 * This method deletes user with given id
	 * 
	 * @param id
	 *            id of required user
	 * @throws DAOException
	 */
	@Override
	public void deleteUser(User user) throws ServiceException {
		try {
			userDao.delete(user.getId());
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}

}
