package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;

/**
 * This command class finishes user session and forwards to the main page.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class LogoutCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";

	/**
	 * This command class finishes user session and forwards to the main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock)
			throws CommandException, InterruptedException {
		request.getSession().invalidate();

		return MAIN_PAGE;
	}

}
