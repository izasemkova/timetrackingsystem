package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;

/**
 * This command class forwards to the tasks list page.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class ManagerTasksPageCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the manager's page
	 */
	private static final String MANAGER_TASKS_PAGE = "/jsp/managertaskspage.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String PROJECT_ID = "projectId";
	private static final String PROJECT = "project";
	private static final String TASKS_LIST = "tasksList";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=manager_tasks_page&projectId=";

	private final static ITaskService taskService = new TaskServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method forwards to the tasks list page. If the user isn't
	 * authorized, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {


		int projectId = Integer.parseInt(request.getParameter(PROJECT_ID));
		request.setAttribute(PROJECT_ID, projectId);

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}

		try {
			List<Task> taskList = taskService.getTasksList(projectId);
			request.setAttribute(TASKS_LIST, taskList);

			Project project = projectService.getProject(projectId);
			request.setAttribute(PROJECT, project);

			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE  + projectId);
		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		return MANAGER_TASKS_PAGE;
	}

}
