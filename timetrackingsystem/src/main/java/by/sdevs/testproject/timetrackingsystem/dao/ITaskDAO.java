package by.sdevs.testproject.timetrackingsystem.dao;

import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.entity.Task;

/**
 * This interface contains methods that bind the task entity with database
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface ITaskDAO {
	
	boolean create(Task newTask, int projectId) throws DAOException;
	
	List<Task> getTasksList(int projectId) throws DAOException;
	
	Task get(int taskId) throws DAOException;
	
	boolean update(Task task, int id) throws DAOException;

	int getProjectId(int taskId) throws DAOException;

}
