package by.sdevs.testproject.timetrackingsystem.service.impl;

import java.util.Collections;
import java.util.List;

import by.sdevs.testproject.timetrackingsystem.dao.IProjectDAO;
import by.sdevs.testproject.timetrackingsystem.dao.exception.DAOException;
import by.sdevs.testproject.timetrackingsystem.dao.impl.ProjectDAOImpl;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for project entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class ProjectServiceImpl implements IProjectService {

	private final static IProjectDAO projectDao = new ProjectDAOImpl();

	private final static ITaskService taskService = new TaskServiceImpl();

	/**
	 * This method creates project using given Project entity
	 * 
	 * @param newProject
	 *            entity for creating Project
	 * @return true if the project has been created, or false otherwise
	 * @throws ServiceException
	 */
	@Override
	public boolean createProject(Project newProject) throws ServiceException {
		try {
			projectDao.create(newProject);
			return true;
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}
	
	/**
	 * This method updates project using given Project entity
	 * 
	 * @param project
	 *            entity for updating Project
	 * @return true if the project has been updated, or false otherwise
	 * @throws ServiceException
	 */
	@Override
	public boolean updateProject(Project project, int id) throws ServiceException {
		try {
			projectDao.update(project, id);
			return true;
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
	}

	/**
	 * This method gets list of projects
	 * 
	 * @return list of project
	 * @throws ServiceException
	 */
	@Override
	public List<Project> getProjectsList() throws ServiceException {
		List<Project> projectsList = Collections.emptyList();
		try {
			projectsList = projectDao.getProjectsList();
			List<Task> tasksList = Collections.emptyList();
			for (Project project : projectsList) {
				tasksList = taskService.getTasksList(project.getId());
				project.setTasksList(tasksList);
			}
		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return projectsList;
	}

	/**
	 * This method gets project with given id
	 * 
	 * @param projectId
	 *            id of required project
	 * @return required project if it exist
	 * @throws ServiceException
	 */
	@Override
	public Project getProject(int projectId) throws ServiceException {
		Project project = null;
		try {
			project = projectDao.get(projectId);
			List<Task> tasksList = taskService.getTasksList(projectId);
			project.setTasksList(tasksList);

		} catch (DAOException e) {
			throw new ServiceException("DAOException: " + e.getMessage(), e);
		}
		return project;
	}

}
