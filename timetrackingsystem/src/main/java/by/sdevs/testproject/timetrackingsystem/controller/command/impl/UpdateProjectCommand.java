package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;

/**
 * This command class forwards to the page for setting responsible person.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class UpdateProjectCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the update project page
	 */
	private static final String UPDATE_PROJECT_PAGE = "/jsp/updateproject.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String PROJECT_ID = "projectId";
	private static final String PROJECT = "project";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=update_project&projectId=";
	
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method forwards to the page for setting responsible person for project. If the user
	 * isn't authorized or doesn't have the manager's permissions, it will
	 * return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		int projectId = Integer.parseInt(request.getParameter(PROJECT_ID));
		
		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}
		
		session.setAttribute(PROJECT_ID, projectId);

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}
		
		try {
			Project project = projectService.getProject(projectId);
			request.setAttribute(PROJECT, project);
		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		request.getSession(true).setAttribute(QUERY_STRING, QUERY_STRING_VALUE + projectId);

		return UPDATE_PROJECT_PAGE;
	}

}
