package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;

/**
 * This command class sets task's responsible and forwards to the manager's page with
 * list of all projects.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class SetTaskResponsibleCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the page for updating project
	 */
	private static final String UPDATE_TASK_PAGE = "/jsp/updatetask.jsp";
	/**
	 * This constant contains path to the page with list of all projects.
	 */
	private static final String MANAGER_PAGE = "/jsp/managertaskspage.jsp";

	private static final String USER = "user";
	private static final String MANAGER = "manager";
	private static final String PROJECT = "project";
	private static final String TASK_ID = "taskId";
	private static final String TASKS_LIST = "tasksList";

	private static final String RESPONSIBLE = "responsible";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=manager_tasks_page&projectId=";

	private static final String UPDATED_TASK_MSG_KEY = "updatedTaskMsg";
	private static final String UPDATED_TASK_MSG_VALUE = "jsp.update.task.error";

	private final static ITaskService taskService = new TaskServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method saves updated task and forwards to the manager's page with
	 * list of all tasks. If the entered information contains errors, it will
	 * return to the page for updating task. If the user isn't authorized
	 * or doesn't have the manager's permissions, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {


		HttpSession session = request.getSession(false);
		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if ((user == null) || !MANAGER.equals(user.getRole())) {
			return MAIN_PAGE;
		}

		int taskId = (Integer) session.getAttribute(TASK_ID);

		String responsible = request.getParameter(RESPONSIBLE);

		String updateTaskMsg = "";

		if (!validationParameters(responsible)) {
			updateTaskMsg = ResourceManager.INSTANCE.getString(UPDATED_TASK_MSG_VALUE);
			request.setAttribute(UPDATED_TASK_MSG_KEY, updateTaskMsg);
			return UPDATE_TASK_PAGE;
		}

		rwlLock.writeLock().lock();
		
		try {
			Task task = taskService.getTask(taskId);
			task.setResponsible(responsible);
			boolean isUpdated = taskService.updateTask(task, taskId);

			if (!isUpdated) {
				updateTaskMsg = ResourceManager.INSTANCE.getString(UPDATED_TASK_MSG_VALUE);
				request.setAttribute(UPDATED_TASK_MSG_KEY, updateTaskMsg);
				return UPDATE_TASK_PAGE;
			}
			
			int projectId = taskService.getProjectId(taskId);
			Project project = projectService.getProject(projectId);
			request.setAttribute(PROJECT, project);
			List<Task> tasksList = taskService.getTasksList(projectId);
			request.setAttribute(TASKS_LIST, tasksList);
			request.removeAttribute(UPDATED_TASK_MSG_KEY);
			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE + projectId);
		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}
		
		rwlLock.writeLock().unlock();
		
		return MANAGER_PAGE;

	}

	private boolean validationParameters(String responsible) {
		if (responsible.isEmpty() || (responsible.length() > 60)) {
			return false;
		}
		return true;
	}
}
