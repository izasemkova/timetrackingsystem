package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.IUserService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.UserServiceImpl;

/**
 * This command class saves new user and forwards to his page.
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class SaveUserCommand implements ICommand {

	/**
	 * This constant contains path to the page for registering new user
	 */
	private static final String REGISTER_PAGE = "/jsp/register.jsp";
	/**
	 * This constant contains path to the user's page.
	 */
	private static final String DEVELOPER_PAGE = "/jsp/developerpage.jsp";
	private static final String MANAGER_PAGE = "/jsp/managerpage.jsp";
	private static final String MAIN_PAGE = "/jsp/mainpage.jsp";
	
	private static final String USER = "user";
	private static final String PROJECTS_LIST = "projectsList";

	private static final String MANAGER = "manager";
	private static final String DEVELOPER = "developer";

	private static final String LOGIN = "login";
	private static final String PASSWORD = "password";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String ROLE = "role";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=";
	
	/**
	 * This constant contains value of command for query string for forwarding to main page 
	 */
	private static final String MAIN_PAGE_COMMAND = "main_page";
	/**
	 * This constant contains value of command for query string for forwarding to administrator's page 
	 */
	private static final String MANAGER_PAGE_COMMAND = "manager_page";
	/**
	 * This constant contains value of command for query string for forwarding to user's page 
	 */
	private static final String DEVELOPER_PAGE_COMMAND = "developer_page";

	private static final String NEW_USER_MSG_KEY = "newUserMsg";
	private static final String NEW_USER_MSG_VALUE = "jsp.register.user.error";
	private static final String USER_EXIST_MSG_VALUE = "jsp.register.user.exist.error";

	private final static IUserService userService = new UserServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method saves new user and forwards to his page. If the entered
	 * information contains errors, it will return to the page for registering
	 * new user.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		String login = request.getParameter(LOGIN);
		String password = request.getParameter(PASSWORD);
		String firstName = request.getParameter(FIRST_NAME);
		String lastName = request.getParameter(LAST_NAME);
		String role = request.getParameter(ROLE);

		String newUserMsg = "";

		if (!validationParameters(login, password, firstName, lastName)) {
			newUserMsg = ResourceManager.INSTANCE.getString(NEW_USER_MSG_VALUE);
			request.setAttribute(NEW_USER_MSG_KEY, newUserMsg);
			return REGISTER_PAGE;
		}

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}

		User newUser = new User();
		newUser.setLogin(login);
		newUser.setPassword(password);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setRole(role);

		try {
			boolean isExist = userService.checkUser(login);
			if (isExist) {
				newUserMsg = ResourceManager.INSTANCE.getString(USER_EXIST_MSG_VALUE);
				request.setAttribute(NEW_USER_MSG_KEY, newUserMsg);
				return REGISTER_PAGE;
			}

			boolean isCreated = userService.createUser(newUser);

			if (!isCreated) {
				newUserMsg = ResourceManager.INSTANCE.getString(NEW_USER_MSG_VALUE);
				request.setAttribute(NEW_USER_MSG_KEY, newUserMsg);
				return REGISTER_PAGE;
			}

			User user = userService.getUser(login, password);

			request.setAttribute(USER, user);

			session = request.getSession(true);
			session.setAttribute(USER, user);
			
			List<Project> projectsList = projectService.getProjectsList();

			request.setAttribute(PROJECTS_LIST, projectsList);

			request.removeAttribute(NEW_USER_MSG_KEY);

			switch (user.getRole()) {
			case MANAGER:
				session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE + MANAGER_PAGE_COMMAND);

				return MANAGER_PAGE;
			case DEVELOPER:
				session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE + DEVELOPER_PAGE_COMMAND);

				return DEVELOPER_PAGE;
			default:
				session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE + MAIN_PAGE_COMMAND);

				return MAIN_PAGE;
			}

		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

	}

	private boolean validationParameters(String login, String password, String firstName, String lastName) {
		if (login.isEmpty()||(login.length()>20)) {
			return false;
		}
		if (password.isEmpty()||(login.length()>20)) {
			return false;
		}
		if (firstName.isEmpty()||(login.length()>20)) {
			return false;
		}
		if (lastName.isEmpty()||(login.length()>30)) {
			return false;
		}
		
		return true;
	}
}
