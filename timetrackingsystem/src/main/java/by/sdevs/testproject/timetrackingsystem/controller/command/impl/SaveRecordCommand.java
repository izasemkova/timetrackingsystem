package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.ITimeRecordService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TimeRecordServiceImpl;

/**
 * This command class saves new record and forwards to the page with list of all
 * records.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class SaveRecordCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the page for adding new record
	 */
	private static final String ADD_NEW_RECORD_PAGE = "/jsp/addrecord.jsp";
	/**
	 * This constant contains path to the page with list of all records.
	 */
	private static final String PAGE = "/jsp/recordslistpage.jsp";

	private static final String USER = "user";
	private static final String PROJECT = "project";
	private static final String TASK = "task";
	private static final String RECORDS_LIST = "recordsList";
	private static final String TASK_ID = "taskId";

	private static final String HOURS = "hours";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=records_list_page&taskId=";

	private static final String NEW_RECORD_MSG_KEY = "newTaskMsg";
	private static final String NEW_RECORD_MSG_VALUE = "jsp.add.task.error";

	private final static ITaskService taskService = new TaskServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();
	private final static ITimeRecordService timeRecordService = new TimeRecordServiceImpl();

	/**
	 * This method saves new record and forwards to the page with list of all
	 * records. If the entered information contains errors, it will return to
	 * the page for adding new task. If the user isn't authorized, it will
	 * return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if (user == null) {
			return MAIN_PAGE;
		}

		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		
		Calendar calendar = Calendar.getInstance();
		Date date = new java.sql.Date(calendar.getTime().getTime());
		
		/*DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		String date = dateFormat.format(new Date());*/
		int hours = 0;

		String newRecordMsg = "";

		try {
			hours = Integer.parseInt(request.getParameter(HOURS));
		} catch (NumberFormatException e) {
			newRecordMsg = ResourceManager.INSTANCE.getString(NEW_RECORD_MSG_VALUE);
			request.setAttribute(NEW_RECORD_MSG_KEY, newRecordMsg);
			return ADD_NEW_RECORD_PAGE;
		}

		int taskId = (Integer) session.getAttribute(TASK_ID);

		try {
			TimeRecord newTimeRecord = new TimeRecord();
			newTimeRecord.setFirstName(firstName);
			newTimeRecord.setLastName(lastName);
			newTimeRecord.setDate(date);
			newTimeRecord.setHours(hours);
			
			boolean isCreated = timeRecordService.createTimeRecord(newTimeRecord, taskId);

			if (!isCreated) {
				newRecordMsg = ResourceManager.INSTANCE.getString(NEW_RECORD_MSG_VALUE);
				request.setAttribute(NEW_RECORD_MSG_KEY, newRecordMsg);
				return ADD_NEW_RECORD_PAGE;
			}

			Task task = taskService.getTask(taskId);
			request.setAttribute(TASK, task);
			int projectId = taskService.getProjectId(taskId);
			Project project = projectService.getProject(projectId);
			request.setAttribute(PROJECT, project);
			List<TimeRecord> timeRecordList = timeRecordService.getTimeRecordsList(taskId);
			request.setAttribute(RECORDS_LIST, timeRecordList);
			request.removeAttribute(NEW_RECORD_MSG_KEY);
			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE + taskId);

		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}
		return PAGE;

	}

}
