package by.sdevs.testproject.timetrackingsystem.entity;

import java.io.Serializable;
import java.util.List;

/**
 * This class is entity that defines the project's parameters
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class Project implements Serializable {
	private static final long serialVersionUID = -5421941498707909267L;
	
	private int id;
	private String name;
	private String responsible;
	private List<Task> tasksList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public List<Task> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", name=" + name + ", responsible=" + responsible + ", tasksList=" + tasksList
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((responsible == null) ? 0 : responsible.hashCode());
		result = prime * result + ((tasksList == null) ? 0 : tasksList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (responsible == null) {
			if (other.responsible != null)
				return false;
		} else if (!responsible.equals(other.responsible))
			return false;
		if (tasksList == null) {
			if (other.tasksList != null)
				return false;
		} else if (!tasksList.equals(other.tasksList))
			return false;
		return true;
	}

}
