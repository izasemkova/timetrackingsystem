package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.Locale;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.controller.util.ResourceManager;

/**
 * This command class changes language for displaying and redirects back to the page.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class ChangeLanguageCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	
	private static final String LANGUAGE = "language";
	private static final String QUERY_STRING = "queryString";
	
	/**
	 * This method changes language for displaying and redirects back to the page using command from query string. 
	 * If the session has finished, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {

		String language = request.getParameter(LANGUAGE);
		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		String queryString = (String) session.getAttribute(QUERY_STRING);
		session.setAttribute(LANGUAGE, language);
		Locale locale = new Locale(language);
		ResourceManager manager = ResourceManager.INSTANCE;
		manager.changeResource(locale);

		return queryString;
	}
}
