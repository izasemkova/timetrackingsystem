package by.sdevs.testproject.timetrackingsystem.service;

import java.util.List;

import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for user entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface IUserService {

	User getUser(String login, String password) throws ServiceException;

	boolean createUser(User newUser) throws ServiceException;
	
	boolean checkUser(String login) throws ServiceException;

	List<User> getUsersList() throws ServiceException;

	User getUser(int userId) throws ServiceException;

	void deleteUser(User user) throws ServiceException;

}

