package by.sdevs.testproject.timetrackingsystem.service;

import java.sql.Date;
import java.util.List;

import by.sdevs.testproject.timetrackingsystem.entity.SumTimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;

/**
 * This interface contains methods for time timeRecord entity
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public interface ITimeRecordService {

	boolean createTimeRecord(TimeRecord newTimeRecord, int taskId) throws ServiceException;

	TimeRecord getTimeRecord(int timeRecordId) throws ServiceException;

	List<TimeRecord> getTimeRecordsList(int taskId) throws ServiceException;

	List<TimeRecord> getTimeRecordsList(Date startDate, Date endDate) throws ServiceException;

	int getTaskId(int id) throws ServiceException;

	List<SumTimeRecord> getTimeList(Date startDate, Date endDate) throws ServiceException;

}
