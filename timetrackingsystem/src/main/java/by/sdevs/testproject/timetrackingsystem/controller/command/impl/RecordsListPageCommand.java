package by.sdevs.testproject.timetrackingsystem.controller.command.impl;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.sdevs.testproject.timetrackingsystem.controller.command.ICommand;
import by.sdevs.testproject.timetrackingsystem.controller.command.exception.CommandException;
import by.sdevs.testproject.timetrackingsystem.entity.Project;
import by.sdevs.testproject.timetrackingsystem.entity.Task;
import by.sdevs.testproject.timetrackingsystem.entity.TimeRecord;
import by.sdevs.testproject.timetrackingsystem.entity.User;
import by.sdevs.testproject.timetrackingsystem.service.IProjectService;
import by.sdevs.testproject.timetrackingsystem.service.ITaskService;
import by.sdevs.testproject.timetrackingsystem.service.ITimeRecordService;
import by.sdevs.testproject.timetrackingsystem.service.exception.ServiceException;
import by.sdevs.testproject.timetrackingsystem.service.impl.ProjectServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TaskServiceImpl;
import by.sdevs.testproject.timetrackingsystem.service.impl.TimeRecordServiceImpl;

/**
 * This command class forwards to the timeRecords list page.
 * 
 * @version 1.0 03 May 2016
 * @author Irina Zasemkova
 */
public class RecordsListPageCommand implements ICommand {

	/**
	 * This constant contains path to main page
	 */
	private static final String MAIN_PAGE = "/jsp/main.jsp";
	/**
	 * This constant contains path to the manager's page
	 */
	private static final String RECORDS_PAGE = "/jsp/recordslistpage.jsp";

	private static final String USER = "user";
	private static final String TASK_ID = "taskId";
	private static final String PROJECT = "project";
	private static final String TASK = "task";
	private static final String RECORDS_LIST = "recordsList";

	private static final String QUERY_STRING = "queryString";
	/**
	 * This constant contains value of query string for changing language
	 */
	private static final String QUERY_STRING_VALUE = "controller?command=records_list_page&taskId=";

	private final static ITimeRecordService timeRecordService = new TimeRecordServiceImpl();
	private final static ITaskService taskService = new TaskServiceImpl();
	private final static IProjectService projectService = new ProjectServiceImpl();

	/**
	 * This method forwards to the timeRecords list page. If the user isn't
	 * authorized, it will return main page.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ReadWriteLock rwlLock) throws CommandException, InterruptedException {


		int taskId = Integer.parseInt(request.getParameter(TASK_ID));
		request.setAttribute(TASK_ID, taskId);

		HttpSession session = request.getSession(false);

		if (session == null) {
			return MAIN_PAGE;
		}

		User user = (User) session.getAttribute(USER);

		if (user == null) {
			return MAIN_PAGE;
		}

		try {
			List<TimeRecord> timeRecordsList = timeRecordService.getTimeRecordsList(taskId);
			request.setAttribute(RECORDS_LIST, timeRecordsList);

			Task task = taskService.getTask(taskId);
			request.setAttribute(TASK, task);
			int projectId = taskService.getProjectId(taskId);
			Project project = projectService.getProject(projectId);
			request.setAttribute(PROJECT, project);

			session.setAttribute(QUERY_STRING, QUERY_STRING_VALUE + taskId);
		} catch (ServiceException e) {
			throw new CommandException("ServiceException: " + e.getMessage(), e);
		}

		return RECORDS_PAGE;
	}

}
