package by.sdevs.testproject.timetrackingsystem.entity;

import java.io.Serializable;

/**
 * This class is entity that defines the sum time record's parameters
 * 
 * @version 1.0 04 May 2016
 * @author Irina Zasemkova
 */
public class SumTimeRecord implements Serializable {
	private static final long serialVersionUID = 3265429796084814638L;
	
	private String firstName;
	private String lastName;
	private int sumHours;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getSumHours() {
		return sumHours;
	}

	public void setSumHours(int sumHours) {
		this.sumHours = sumHours;
	}

	@Override
	public String toString() {
		return "SumTimeRecord [firstName=" + firstName + ", lastName=" + lastName + ", sumHours=" + sumHours + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + sumHours;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SumTimeRecord other = (SumTimeRecord) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (sumHours != other.sumHours)
			return false;
		return true;
	}

}
