package by.sdevs.testproject.timetrackingsystem.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This class sets request and response encoding in UTF-8
 * 
 * @version 1.0 02 May 2016
 * @author Irina Zasemkova
 */
public class CharacterEncodingFilter implements Filter {

	/**
	 * This constant contains reference to encoding in web.xml
	 */
	private static final String ENCODING = "encoding";

	private String code;

	/**
	 * This method resets encoding when filter is disconnecting
	 */
	@Override
	public void destroy() {
		code = null;
	}

	/**
	 * This method sets encoding for request and response
	 * 
	 * @param request
	 *            HttpServletRequest from the client.
	 * @param response
	 *            HttpServletResponse to the client.
	 * @param chain
	 *            this is chain of filters
	 * @throws ServletException,
	 *             IOException
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String requestEncoding = request.getCharacterEncoding();
		if (code != null && !code.equalsIgnoreCase(requestEncoding)) {
			request.setCharacterEncoding(code);
			response.setCharacterEncoding(code);
		}
		chain.doFilter(request, response);
	}

	/**
	 * This method sets encoding when filter is connecting
	 * 
	 * @param filterConfig
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		code = filterConfig.getInitParameter(ENCODING);
	}

}
